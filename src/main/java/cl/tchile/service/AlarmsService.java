package cl.tchile.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tchile.ConstantsFatem;
import cl.tchile.controller.model.FiltroAlarma;
import cl.tchile.controller.model.FiltroHistorialAlarma;
import cl.tchile.controller.model.RespuestaAlarma;
import cl.tchile.controller.model.RespuestaDetalleAlarma;
import cl.tchile.controller.model.RespuestaHistoricoAlarma;
import cl.tchile.dao.AlarmaCrudDAO;
import cl.tchile.dao.AlarmaDAO;
import cl.tchile.dao.exception.FTMException;
import cl.tchile.model.FtmAlarma;
import cl.tchile.model.HistoricoAlarma;
import cl.tchile.util.UtilDate;

/**
 * Clase AlarmsService servicio que se conecta con capa DAO para obtener datos de alarmas.
 * @author Indra 
 */
@Service
public class AlarmsService {

	private static final Logger LOGGER = Logger.getLogger(AlarmsService.class);
	
	
	@Autowired
	AlarmaDAO alarmaDao;
	
	@Autowired
	AlarmaCrudDAO alarmaCrudDAO;
	
	public RespuestaAlarma getAlarms(FiltroAlarma filtros) throws FTMException {
		RespuestaAlarma resp = new RespuestaAlarma();
		ArrayList<RespuestaDetalleAlarma> list = new ArrayList<RespuestaDetalleAlarma>();
		List<FtmAlarma> listAlarm = null;
				
		String query = filtros.getQuery();
		String zona = filtros.getZona();
		String planta = filtros.getPlanta();
		String elemento = filtros.getElemento();
		
		if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONA)) {
			listAlarm = alarmaDao.getAlarmsFiltersZona(zona);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_PLANTA)) {
			listAlarm = alarmaDao.getAlarmsFiltersPta(planta);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ELEMENTO)) {
			listAlarm = alarmaDao.getAlarmsFiltersElemento(elemento);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAPLANTA)) {
			listAlarm = alarmaDao.getAlarmsFiltersZonaPta(zona, planta);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAELEMENTO)) {
			listAlarm = alarmaDao.getAlarmsFiltersZonaElemnt(zona, elemento);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_PLANTAELEMENTO)) {
			listAlarm = alarmaDao.getAlarmsFiltersPtaElement(planta, elemento);
		}else if(query.equals(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAPLANTAELEMENTO)) {
			listAlarm = alarmaDao.getAlarmsFiltersZonaPtaElement(zona, planta, elemento);
		}
		LOGGER.info("Execute query "+query);
		for(FtmAlarma al:listAlarm) {
			RespuestaDetalleAlarma detAla = new RespuestaDetalleAlarma();
			detAla.setId(al.getIdAlarma()+"");
			detAla.setFechaEvento(UtilDate.parseDateTms(al.getFechaInicio()));
			detAla.setGravedad(al.getSlot()+"");
			detAla.setZona(al.getTipoAlarma());
			detAla.setElemento(al.getNombreInterno());
			detAla.setDiagnostico(al.getPortpon()+"");
			detAla.setPlanta(al.getPta());
			detAla.setNombreElemento(al.getNaname());
			detAla.setTotalCliente(al.getFtmDetalleAlarmas().size()+"");
			list.add(detAla);
		}
		resp.setListDetalleAlarma(list);
		
		return resp;
		
	}
	
	
	
	
	
	public List<RespuestaHistoricoAlarma> getHistoryAlarms(FiltroHistorialAlarma filtros) throws FTMException {
				
		List<HistoricoAlarma> list = alarmaCrudDAO.getAlarmsByDate(filtros);		
		
		return null;
	}
	
	
}
