package cl.tchile.service;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.tchile.controller.model.RespuestaLogin;
import cl.tchile.controller.model.Usuario;
import cl.tchile.dao.UsuarioDAO;
import cl.tchile.dao.exception.FTMException;
import cl.tchile.model.FtmUsuario;
import cl.tchile.util.UtilDate;
import cl.tchile.util.UtilNumeric;
import cl.tchile.util.UtilString;

/**
 * Clase LoginService servicio que se conecta con capa DAO para obtener datos de usuario.
 * @author Indra 
 */
@Service
public class LoginService {

	private static final Logger LOGGER = Logger.getLogger(LoginService.class);
	
	
	@Autowired
	UsuarioDAO userDao;
	
	
	/**
	 * Obtiene data de usuario conectandose con userDao
	 * @param user
	 * @return RespuestaLogin
	 * @throws FTMException
	 */
	 public RespuestaLogin getDataUserLogin(Usuario user) throws FTMException {
		 LOGGER.info("init LoginService");
		 RespuestaLogin resp = null;
		 try {

			 FtmUsuario us = userDao.findOne(UtilNumeric.parseLong(user.getUser()));
			 //FtmUsuario us = userDao.findUserAcces(UtilNumeric.parseLong(user.getUser()));
			 if(us!=null) {
				 resp = new RespuestaLogin();
				 resp.setUser(us.getNombre());
				 resp.setPerfil(UtilString.noNull(us.getFtmPerfil().getNombre()));
				 resp.setDateConection(UtilDate.formatDate(new Date()));
			 }
			 
		 }catch(Exception e ) {
			 e.printStackTrace();
		 }
		
		 
		 return resp;
	 }
     
     
}
