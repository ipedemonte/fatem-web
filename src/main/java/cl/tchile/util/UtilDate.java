/**
 * Clase que contiene metodos de conversion y formateo de fechas.
 */
package cl.tchile.util;

import java.util.Date;
import java.util.TimeZone;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 *
 * @author Indra
 */
public class UtilDate {
    
    public static final String DATE_FORMAT_1 = "dd-MM-yyyy";
    public static final String DATE_FORMAT_7 = "dd-MM-yyyy HH:mm:ss";
	public static final String DATE_FORMAT_8 = "dd-MM-yyyy HH:mm";
	
	public static final String DATE_FORMAT_2 = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DATE_FORMAT_3 = "yyyy-MM-dd'Z'";
	public static final String DATE_FORMAT_4 = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	public static final String DATE_FORMAT_5 = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_6 = "yyyy-MM-dd";
    public static final String UTC = "UTC";
	

    /**
     * Formatea fecha con formato dd-MM-yyyy
     * @param date
     * @return 
     */
    public static String formatDate(Date date){
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);
		dateFormat.setTimeZone(TimeZone.getTimeZone(UTC));		
		String dateText = dateFormat.format(date);
			
		return dateText;
    }
	
    /**
     * Parsea fecha de String a Date con formato dd-MM-yyyy
     * @param date
     * @return 
     */
    public static Date parseDate(String date){
		
		Date dateNew = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_1);	
		try {
			dateNew = dateFormat.parse(date);
		} catch (ParseException e) {
	        System.out.println("Error parsing date" + e);
		}		
		return dateNew;
    }
	
    /**
     * Parsea fecha de String a Date
     * @param date
     * @return 
     */
    public Date parseDateNotFormat(String date){
		
	Date dateNew = null;
	SimpleDateFormat dateFormat = new SimpleDateFormat();	
	try {
            dateNew = dateFormat.parse(date);
	} catch (ParseException e) {
            System.out.println("Error parsing date without no format" + e);
	}	
	return dateNew;
    }
	
    /**
     * 
     * @param date
     * @param format
     * @return 
     */
    public Date parseDateWhitFormat(String date, String format){		
        Date dateNew = null;
	SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	try {
            dateNew = dateFormat.parse(date);
	} catch (ParseException e) {
            System.out.println("Error parsing date with format" + e);
	}	
	return dateNew;
    }
	
    /**
     * Parsea fecha de String a String con formato enviado por parametro
     * @param date
     * @param format
     * @return 
     */
    public String parseDate(String date, String format){
	Date dateP = parseDateWhitFormat(date, DATE_FORMAT_8);
	String dateNew = "";
	if(dateP != null){
            dateNew = formatDate(dateP, format);
	}
	return dateNew;
    }	
	
    /**
     * Parsea fecha de Date a String con formato enviado por parametro
     * @param date
     * @param format
     * @return 
     */
    public static String formatDate(Date date, String format){		
	SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	dateFormat.setTimeZone(TimeZone.getDefault());	
	String dateNew = dateFormat.format(date);

        return dateNew;
    }
	
    /**
     * Parsea fecha de Date a XMLGregorianCalendar con formato yyyy-MM-dd
     * @param date
     * @return 
     */
    public XMLGregorianCalendar dateXMLGregorianCalendar(Date date) {
	XMLGregorianCalendar gDateFormatted = null;
        try {
            DateFormat format = new SimpleDateFormat(DATE_FORMAT_6);
	    gDateFormatted = DatatypeFactory.newInstance().newXMLGregorianCalendar(format.format(date));
	} catch (DatatypeConfigurationException e) {
            System.out.println("Error parser getHoratoGregorian"+e);
	}   
        return gDateFormatted;
    }
        
       
    /**
     * Parsea fecha de Date a JAXBElement<XMLGregorianCalendar>
     * @param date
     * @return 
     */
    public JAXBElement<XMLGregorianCalendar> dateXMLGregJAXBCalendar(Date date) {		
		
	XMLGregorianCalendar xmlGregorianCalendar = dateXMLGregorianCalendar(date);
	JAXBElement<XMLGregorianCalendar> gDateFormatted = new JAXBElement<>(new QName("http://statusrequest.core.schema.ultra-as.com"),XMLGregorianCalendar.class, xmlGregorianCalendar);   
        return gDateFormatted;
    }
    
    
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        UtilDate util = new UtilDate();
        Date forDate = util.parseDateWhitFormat("04JAN19", "ddMMMyy");
        System.out.println("parse 2 "+forDate);
        String dateFormat = util.formatDate(forDate, DATE_FORMAT_1);
        System.out.println("formatDate 2 "+dateFormat);
	
    }
    
    
    public Date formatDate2(String date){
		
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat dateFormat = new SimpleDateFormat("ddmmmyy");
	Date dateToday = null;
	String newDate = getDateTraslate(date);	
	try {
            dateToday = dateFormat.parse(newDate);
            cal.setTime(dateToday);			
	} catch (ParseException e) {
            System.out.println("Parse date failed "+date);
	}		
        return cal.getTime();
    }
    
    
    private String getDateTraslate(String date){
		
		String newDate = null;
		
		switch(date.substring(2 , 5)){
			case "JAN":
				newDate = date.replace("JAN", "ENE");
				break;
			case "APR":
				newDate = date.replace("APR", "ABR");
				break;
			case "AUG":
				newDate = date.replace("AUG", "AGO");
				break;
			case "DEC":
				newDate = date.replace("DEC", "DIC");
				break;
			default:
				newDate = date;
		}
		
		return newDate;
	}

	public static String parseDateTms(Timestamp fechaInicio) {
		Date date = new Date(fechaInicio.getTime());
		String dt = formatDate(date, DATE_FORMAT_8);
		return dt;
	}
    
    
}
