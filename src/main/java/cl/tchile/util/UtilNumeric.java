/**
 * Clase que contiene metodos de conversion y formateo de fechas.
 */
package cl.tchile.util;

import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 *
 * @author Indra
 */
public class UtilNumeric {
   
	

    public static Long parseLong(String data) {
    	Long dataLong = null;
    	try {
             dataLong = new Long(data);           
         } catch(Exception ex) {
             System.out.println(ex.getMessage());
         }
    	 return dataLong;
    }
    
    
}
