/*
 * Utilizado para operar con caracteres.
 */
package cl.tchile.util;

/**
 *
 * @author Indra
 */
public class UtilString {
    
    
   /**
   * Este m&eacute;todo llena el <code>String</code> recibido con espacios
   * en blanco hasta completar el largo recibido.
   * @param strIn String para llenar con espacios.
   * @param largo Largo total del string que debe ser retornado
   * @return string de largo <B>largo</B> con sus ultimos caracteres vacios
   */
    public static String fillSpaces(String strIn, int largo) {
        String str = null;
        StringBuffer paddedString = new StringBuffer(largo);
        synchronized (paddedString) {
            paddedString.append(strIn);
            if (paddedString.length() > largo){
                paddedString.setLength(largo);
            } else {  // Is filled with white spaces.
                int  n = largo - paddedString.length();
                while (n > 0) {
                    paddedString.append(' ');
                    n--;
                }
            }
            str = paddedString.toString();
        }
        return str;
    }

    /**
     * Este m&eacute;todo llena el <code>String</code> recibido con ceros
     * hasta completar el largo recibido.
     * @param strIn String para llenar con ceros.
     * @param largo Largo total del string que debe ser retornado
     * @return string de largo <B>largo</B> con sus ultimos caracteres ceros
     */
    public static String fillZeros(String strIn, int largo) {
        String str = null;
        StringBuffer paddedString = new StringBuffer(largo);
        synchronized (paddedString) {
            paddedString.append(strIn);
            if (paddedString.length() > largo) {
                paddedString.setLength(largo);
            } else {  // Is filled with white spaces.
                int  n = largo - paddedString.length();
                while (n > 0) {
                    paddedString.insert(0, '0');
                    n--;
                }
            }
            str = paddedString.toString();
        }
        return str;
    }

    /**
     * Este metodo reemplaza una palabra por otra dentro de un String.
     * Este metodo reemplaza todas las ocurrencias del String especificado.
     * @param strIn String para reemplazar internamente con la palabra especificada
     * @param oldWord String a ser reemplazado
     * @param newWord nuevo String para poner donde el oldWord esta localizado
     * @return Un String con las palabras reemplazadas
     */
    public static String replaceString(
        String strIn, String oldWord, String newWord) {
        if ((strIn != null) && (oldWord != null) && (newWord != null)) {
            int position = strIn.indexOf(oldWord);
            if (position >= 0) {
                StringBuffer result = new StringBuffer(strIn.substring(0, position));
                result.append(newWord);
                result.append(strIn.substring(position + oldWord.length()));
                return replaceString(result.toString(), oldWord, newWord);
            } else {
                return strIn;
            }
        } else {
            throw new NullPointerException("Existe argumento nulo");
        }
    }

    public static boolean isNumeric(String strNumero) {
        try {
            new Double(strNumero);
            return(true);
        } catch(Exception ex) {
            return(false);
        }
    }

    public static String truncate(String strIn, int largoMax) {
        String strOut= null;
        if(strIn!=null){
            int largoStrIn = strIn.length();
            System.out.println("----->EL LARGO DEL STRING IN: "+largoStrIn);
            if(largoStrIn >largoMax){
                strOut = strIn.substring(0,largoMax);
                System.out.println("----->STRING TRUNCADO: "+strOut);
            }else{
                strOut =strIn;
            }
        }
        return strOut;
    }

    public static String noNull(String str) {
        return str == null ? "" : str;
    }

    public static String noNull(String str, String def) {
        return str == null ? def : str;
    }
    
    public static String rtrim(String s) {
        int i = s.length()-1;
        while (i >= 0 && Character.isWhitespace(s.charAt(i))) {
            i--;
        }
        return s.substring(0,i+1);
    }

    public static String ltrim(String s) {
        int i = 0;
        while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
            i++;
        }
        return s.substring(i);
    }
    
    public static String fullTrim(String s) {
    	return ltrim(rtrim(s));
    }
    
}
