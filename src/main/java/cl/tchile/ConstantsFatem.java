package cl.tchile;

public class ConstantsFatem {

	public static final String ERROR_DASHBOARD_ALARMA_EMPTY_FILTER = "-1";
	
	public static final String DASHBOARD_ALARMA_QUERY_ZONA = "1";
	public static final String DASHBOARD_ALARMA_QUERY_PLANTA = "2";
	public static final String DASHBOARD_ALARMA_QUERY_ELEMENTO = "3";
	
	public static final String DASHBOARD_ALARMA_QUERY_ZONAPLANTA = "4";
	public static final String DASHBOARD_ALARMA_QUERY_ZONAELEMENTO = "5";
	
	public static final String DASHBOARD_ALARMA_QUERY_PLANTAELEMENTO = "6";
	
	public static final String DASHBOARD_ALARMA_QUERY_ZONAPLANTAELEMENTO = "7";
}
