package cl.tchile.controller;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.tchile.ConstantsFatem;
import cl.tchile.controller.model.FiltroAlarma;
import cl.tchile.controller.model.RespuestaAlarma;
import cl.tchile.dao.exception.FTMException;
import cl.tchile.service.AlarmsService;
import cl.tchile.util.UtilString;



/**
 * Clase AlarmReportController expone servicio rest para generar reporte alarma fatem.
 * @author Indra 
 */
@RestController
@RequestMapping("/fatem/alarm/api")
public class AlarmReportController {

	private static final Logger LOGGER = Logger.getLogger(AlarmReportController.class);
	
	@Autowired
	AlarmsService alarmService;
	
	ResponseEntity<RespuestaAlarma> reply = null;
	
	@RequestMapping(value = "/getAlarms", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <RespuestaAlarma>  getAlarms(@RequestBody FiltroAlarma filtros) {
		LOGGER.info("init AlarmReportController");
		RespuestaAlarma resp;
		try {
			filtros = this.validaFiltros(filtros);
			if(filtros.getQuery().equals(ConstantsFatem.ERROR_DASHBOARD_ALARMA_EMPTY_FILTER)) {
				LOGGER.error("FTMException "+HttpStatus.BAD_REQUEST+" E: ");
				resp = new RespuestaAlarma();
				reply = new ResponseEntity<RespuestaAlarma>(resp, HttpStatus.BAD_REQUEST);
			}else {						
				resp = alarmService.getAlarms(filtros);				
				reply = new ResponseEntity<RespuestaAlarma>(resp, HttpStatus.OK);	
			}
		}catch(FTMException fe) {
			LOGGER.error("FTMException "+HttpStatus.INTERNAL_SERVER_ERROR+" E: "+fe.getMessage());
			resp = new RespuestaAlarma();
			reply = new ResponseEntity<RespuestaAlarma>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LOGGER.info("end AlarmReportController");
		return reply;
	}

	private FiltroAlarma validaFiltros(FiltroAlarma filtros) {
		String zona = filtros.getZona();
		String planta = filtros.getPlanta();
		String elemento = filtros.getElemento();
		zona = UtilString.fullTrim(UtilString.noNull(zona));
		planta = UtilString.fullTrim(UtilString.noNull(planta));
		elemento = UtilString.fullTrim(UtilString.noNull(elemento));
		
		filtros.setElemento(elemento);
		filtros.setPlanta(planta);
		filtros.setZona(zona);
		
		if(zona.isEmpty() && planta.isEmpty() && elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.ERROR_DASHBOARD_ALARMA_EMPTY_FILTER);
		}else if(!zona.isEmpty() && planta.isEmpty() && elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONA);
		}else if(zona.isEmpty() && !planta.isEmpty() && elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_PLANTA);
		}else if(zona.isEmpty() && planta.isEmpty() && !elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ELEMENTO);
		}else if(!zona.isEmpty() && !planta.isEmpty() && elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAPLANTA);
		}else if(!zona.isEmpty() && planta.isEmpty() && !elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAELEMENTO);
		}else if(zona.isEmpty() && !planta.isEmpty() && !elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_PLANTAELEMENTO);
		}else if(!zona.isEmpty() && !planta.isEmpty() && !elemento.isEmpty()) {
			filtros.setQuery(ConstantsFatem.DASHBOARD_ALARMA_QUERY_ZONAPLANTAELEMENTO);
		}
		
		return filtros;
	}
	
}
