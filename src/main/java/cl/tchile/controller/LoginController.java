package cl.tchile.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.tchile.controller.model.RespuestaLogin;
import cl.tchile.controller.model.Usuario;
import cl.tchile.dao.exception.FTMException;
import cl.tchile.service.LoginService;
import cl.tchile.util.UtilString;

/**
 * Clase LoginController expone servicio rest para login de usuario fatem.
 * @author Indra 
 */
@RestController
@CrossOrigin
//@CrossOrigin(origins="${spring.mail.host}"+"${server.port}", maxAge=3600)
//@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/fatem/login/api")
public class LoginController {
	
	private static final Logger LOGGER = Logger.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService;
	

	/**
	 * metodo login que valida datos usuario, conecta a LDAP(pendiente) y retorna respuesta.
	 * @param user
	 * @return ResponseEntity<RespuestaLogin> 
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RespuestaLogin>  login(@RequestBody Usuario user) {
		LOGGER.info("init LoginController");
		
		ResponseEntity<RespuestaLogin> reply = null;
		
		if(!this.validateData(user)) {
			reply = new ResponseEntity<RespuestaLogin>(new RespuestaLogin(), HttpStatus.BAD_REQUEST);
			return reply;
		}
		
		try {
			RespuestaLogin resp = loginService.getDataUserLogin(user);
			if(resp != null) {	
				reply = new ResponseEntity<RespuestaLogin>(resp, HttpStatus.OK);
				LOGGER.info("loginService "+HttpStatus.OK);
			}else {
				resp = new RespuestaLogin();
				reply = new ResponseEntity<RespuestaLogin>(resp, HttpStatus.BAD_REQUEST);
				LOGGER.info("loginService "+HttpStatus.BAD_REQUEST);
			}
		}catch(FTMException fe) {
			LOGGER.error("FTMException "+HttpStatus.INTERNAL_SERVER_ERROR+" E: "+fe.getMessage());
			RespuestaLogin resp = new RespuestaLogin();
			reply = new ResponseEntity<RespuestaLogin>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		LOGGER.info("end loginService ");
		return reply;
		
	}

	/**
	 * Valida no null y vacio de username y password
	 * @param user
	 * @return boolean
	 */
	private boolean validateData(Usuario user) {
		String username = user.getUser();
		String password = user.getPassword();
		username = UtilString.fullTrim(UtilString.noNull(username));
		password = UtilString.fullTrim(UtilString.noNull(password));
		if(username.isEmpty() || user.getPassword().isEmpty()) {			
			return false;
		}else {
			user.setUser(username);
			user.setPassword(password);
		}
		return true;
		
	}
	

}
