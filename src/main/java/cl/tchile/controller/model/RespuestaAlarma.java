package cl.tchile.controller.model;

import java.io.Serializable;
import java.util.List;

public class RespuestaAlarma implements Serializable{

	private static final long serialVersionUID = 1L;
	
	List<RespuestaDetalleAlarma> listDetalleAlarma;


	public List<RespuestaDetalleAlarma> getListDetalleAlarma() {
		return listDetalleAlarma;
	}

	public void setListDetalleAlarma(List<RespuestaDetalleAlarma> listDetalleAlarma) {
		this.listDetalleAlarma = listDetalleAlarma;
	}
	
	
	
}
