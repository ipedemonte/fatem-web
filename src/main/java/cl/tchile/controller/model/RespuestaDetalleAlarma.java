package cl.tchile.controller.model;

import java.io.Serializable;

/**
 * Clase de respuesta JSON
 * @author Indra
 *
 */
public class RespuestaDetalleAlarma implements Serializable{


	private static final long serialVersionUID = 1L;

	private String id;
	
	private String fechaEvento;
	
	private String gravedad;
	
	private String zona;
	
	private String elemento;
	
	private String diagnostico;
	
	private String planta;
	
	private String nombreElemento;
	
	private String totalCliente;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFechaEvento() {
		return fechaEvento;
	}

	public void setFechaEvento(String fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	public String getGravedad() {
		return gravedad;
	}

	public void setGravedad(String gravedad) {
		this.gravedad = gravedad;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getElemento() {
		return elemento;
	}

	public void setElemento(String elemento) {
		this.elemento = elemento;
	}

	public String getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getNombreElemento() {
		return nombreElemento;
	}

	public void setNombreElemento(String nombreElemento) {
		this.nombreElemento = nombreElemento;
	}

	public String getTotalCliente() {
		return totalCliente;
	}

	public void setTotalCliente(String totalCliente) {
		this.totalCliente = totalCliente;
	}
	
	
	
	

}
