package cl.tchile.controller.model;

import java.io.Serializable;


public class FiltroHistorialAlarma implements Serializable {
	

	private static final long serialVersionUID = 1L;

	private String fechaInicio;
	
	private String fechaFin;

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	

}
