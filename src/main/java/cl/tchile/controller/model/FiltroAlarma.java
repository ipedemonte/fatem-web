package cl.tchile.controller.model;

import java.io.Serializable;

/**
 * Clase de entrada parametros JSON
 * @author Indra
 *
 */
public class FiltroAlarma implements Serializable{

	
	private static final long serialVersionUID = 1L;

	private String zona;
	
	private String planta;
	
	private String elemento;
	
	private String query;
	
	

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getElemento() {
		return elemento;
	}

	public void setElemento(String elemento) {
		this.elemento = elemento;
	}
	
	
}
