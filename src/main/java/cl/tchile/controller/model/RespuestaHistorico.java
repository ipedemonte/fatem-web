package cl.tchile.controller.model;

import java.io.Serializable;
import java.util.List;

public class RespuestaHistorico implements Serializable{

	private static final long serialVersionUID = 1L;
	
	List<RespuestaHistoricoAlarma> listHistoricoAlarma;

	public List<RespuestaHistoricoAlarma> getListHistoricoAlarma() {
		return listHistoricoAlarma;
	}

	public void setListHistoricoAlarma(List<RespuestaHistoricoAlarma> listHistoricoAlarma) {
		this.listHistoricoAlarma = listHistoricoAlarma;
	}


	
	
}
