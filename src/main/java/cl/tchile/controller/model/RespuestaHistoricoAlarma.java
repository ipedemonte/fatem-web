package cl.tchile.controller.model;

import java.io.Serializable;

/**
 * Clase de respuesta JSON
 * @author Indra
 *
 */
public class RespuestaHistoricoAlarma implements Serializable{


	private static final long serialVersionUID = 1L;

	private String zona;
	
	private String planta;
	
	private String elemento;
	
	private String hostName;
	
	private String summary;
	
	private String cantidad;
	
	
	
	

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public void setElemento(String elemento) {
		this.elemento = elemento;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getPlanta() {
		return planta;
	}

	public void setPlanta(String planta) {
		this.planta = planta;
	}

	public String getElemento() {
		return elemento;
	}
}
