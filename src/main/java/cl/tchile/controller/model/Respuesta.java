package cl.tchile.controller.model;

import java.io.Serializable;

/**
 * Clase de respuesta JSON
 * @author Indra
 *
 */
public class Respuesta implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String status;
	private String message;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	

}
