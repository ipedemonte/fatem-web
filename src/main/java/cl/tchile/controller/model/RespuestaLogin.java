package cl.tchile.controller.model;

import java.io.Serializable;

/**
 * Clase de respuesta JSON
 * @author Indra
 *
 */
public class RespuestaLogin implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private String user;
	private String perfil;
	private String dateConection;
	
	
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getDateConection() {
		return dateConection;
	}
	public void setDateConection(String dateConection) {
		this.dateConection = dateConection;
	}
	
	
	
}
