package cl.tchile.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.tchile.controller.model.FiltroHistorialAlarma;
import cl.tchile.controller.model.RespuestaHistorico;
import cl.tchile.controller.model.RespuestaHistoricoAlarma;
import cl.tchile.dao.exception.FTMException;
import cl.tchile.service.AlarmsService;

/**
 * Clase HistoricoAlarmaController expone servicio rest para generar reporte historico alarma fatem.
 * @author Indra 
 */
@RestController
@RequestMapping("/fatem/history/api")
public class HistoricoAlarmaController {

private static final Logger LOGGER = Logger.getLogger(AlarmReportController.class);
	
	@Autowired
	AlarmsService alarmService;
	
	ResponseEntity<RespuestaHistorico> reply = null;
	
	@RequestMapping(value = "/getHistoryAlarms", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity <RespuestaHistorico>  getHistoryAlarms(@RequestBody FiltroHistorialAlarma filtros) {
		LOGGER.info("init HistoricoAlarmaController");
		
		RespuestaHistorico resp;
		try {
			List<RespuestaHistoricoAlarma> list = alarmService.getHistoryAlarms(filtros);
			resp = new RespuestaHistorico();
			resp.setListHistoricoAlarma(list);
			reply = new ResponseEntity<RespuestaHistorico>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		
		}catch(FTMException fe) {
			LOGGER.error("FTMException "+HttpStatus.INTERNAL_SERVER_ERROR+" E: "+fe.getMessage());
			resp = new RespuestaHistorico();
			reply = new ResponseEntity<RespuestaHistorico>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		return reply;
	
		
	}
}
