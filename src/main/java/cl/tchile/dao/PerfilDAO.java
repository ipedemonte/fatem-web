package cl.tchile.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.tchile.dao.exception.FTMException;
import cl.tchile.model.FtmPerfil;

public interface PerfilDAO extends JpaRepository<FtmPerfil, Long>{
	
	public FtmPerfil findOne(long id) throws FTMException;

}
