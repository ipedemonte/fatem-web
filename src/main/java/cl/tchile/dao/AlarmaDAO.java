package cl.tchile.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tchile.dao.exception.FTMException;
import cl.tchile.model.FtmAlarma;

/**
 * Interfaz AlarmaDAO contiene metodos para ejecutar querys de tabla alarma.
 * @author Indra
 *
 */
@Repository
public interface AlarmaDAO extends JpaRepository<FtmAlarma, String>{
	
	public List<FtmAlarma> getAlarmsFiltersPta(String pta)throws FTMException; 
	
	public List<FtmAlarma> getAlarmsFiltersElemento(String element)throws FTMException; 
	
	public List<FtmAlarma> getAlarmsFiltersZona(String zona)throws FTMException; 
	
	
	
	public List<FtmAlarma>getAlarmsFiltersZonaPta(String zona, String pta)throws FTMException; 
	
	public List<FtmAlarma>getAlarmsFiltersZonaElemnt(String zona, String element)throws FTMException;
	
	public List<FtmAlarma>getAlarmsFiltersPtaElement(String pta, String element)throws FTMException;
	
	
	
	public List<FtmAlarma>getAlarmsFiltersZonaPtaElement(String zona, String pta, String element)throws FTMException;

}
