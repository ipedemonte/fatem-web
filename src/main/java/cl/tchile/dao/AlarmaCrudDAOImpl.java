package cl.tchile.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import cl.tchile.controller.model.FiltroHistorialAlarma;
import cl.tchile.controller.model.RespuestaHistoricoAlarma;
import cl.tchile.model.HistoricoAlarma;

@Repository
@Transactional
public class AlarmaCrudDAOImpl implements AlarmaCrudDAO{

	
    private static final String QUERY_HISTORICO = "SELECT ID_ALARMA,  NOMBRE_INTERNO, PTA, NANAME, SLOT, PORTPON FROM FTM_ALARMA where FECHA_INICIO >= to_timestamp(?1, 'dd-mm-yyyy hh24:mi:ss')  and FECHA_FIN_ESTIMADO <= to_timestamp(?2, 'dd-mm-yyyy hh24:mi:ss')";
	@PersistenceContext
    private EntityManager em;

    

	public List<HistoricoAlarma> getAlarmsByDate(FiltroHistorialAlarma filtro){
		
			Query query = em.createNativeQuery(QUERY_HISTORICO);
	        query.setParameter(1, "16-08-2019 00:00:00");
	        query.setParameter(2, "20-08-2019 00:00:00");
	        List<HistoricoAlarma> list = query.getResultList();
	        return list;
		
	}



	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}



	@Override
	public void delete(FiltroHistorialAlarma arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void delete(HistoricoAlarma arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void delete(Iterable<? extends HistoricoAlarma> arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public boolean exists(FiltroHistorialAlarma arg0) {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public Iterable<HistoricoAlarma> findAll() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public Iterable<HistoricoAlarma> findAll(Iterable<FiltroHistorialAlarma> arg0) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public HistoricoAlarma findOne(FiltroHistorialAlarma arg0) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public <S extends HistoricoAlarma> S save(S arg0) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public <S extends HistoricoAlarma> Iterable<S> save(Iterable<S> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

   
}
