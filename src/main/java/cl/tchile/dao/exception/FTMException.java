package cl.tchile.dao.exception;

public class FTMException extends Exception {

	private static final long serialVersionUID = 8639184549458422203L;
	private final String msg;
	private final Integer code;
	public static final Integer JPA_ERROR_CODE=500;
	public static final Integer BUSINESS_ERROR_CODE=400;


	/**
	 * @param msg
	 * @param code
	 */
	public FTMException(String msg, Integer code) {
		super();
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public Integer getCode() {
		return code;
	}

}
