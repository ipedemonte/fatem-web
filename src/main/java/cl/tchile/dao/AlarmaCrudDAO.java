package cl.tchile.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.tchile.controller.model.FiltroHistorialAlarma;
import cl.tchile.model.HistoricoAlarma;

/**
 * Interfaz AlarmaCrudDAO contiene metodos para ejecutar querys de tabla alarma.
 * @author Indra
 *
 */
@Repository
public interface AlarmaCrudDAO extends CrudRepository<HistoricoAlarma, FiltroHistorialAlarma>{
	
	public List<HistoricoAlarma> getAlarmsByDate(FiltroHistorialAlarma filtro);

}
