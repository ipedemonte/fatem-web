package cl.tchile.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.tchile.dao.exception.FTMException;
import cl.tchile.model.FtmUsuario;
/**
 * Interfaz UsuarioDAO contiene metodos basicos CRUD.
 * @author Indra
 *
 */
@Repository
public interface UsuarioDAO extends JpaRepository<FtmUsuario, Long>{

	public FtmUsuario findOne(long user) throws FTMException;
	
	public FtmUsuario findUserAcces(long id) throws FTMException;
	
	//public FtmUsuario save(FtmUsuario user);
	
	//public void delete(FtmUsuario user);
	
	//public FtmUsuario update(FtmUsuario user);
	
	
}
