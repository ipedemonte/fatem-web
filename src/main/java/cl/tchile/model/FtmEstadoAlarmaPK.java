package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the FTM_ESTADO_ALARMA database table.
 * 
 */
@Embeddable
public class FtmEstadoAlarmaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_ESTADO")
	private long idEstado;

	@Column(name="ID_ALARMA")
	private long idAlarma;

	public FtmEstadoAlarmaPK() {
	}
	public long getIdEstado() {
		return this.idEstado;
	}
	public void setIdEstado(long idEstado) {
		this.idEstado = idEstado;
	}
	public long getIdAlarma() {
		return this.idAlarma;
	}
	public void setIdAlarma(long idAlarma) {
		this.idAlarma = idAlarma;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FtmEstadoAlarmaPK)) {
			return false;
		}
		FtmEstadoAlarmaPK castOther = (FtmEstadoAlarmaPK)other;
		return 
			(this.idEstado == castOther.idEstado)
			&& (this.idAlarma == castOther.idAlarma);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idEstado ^ (this.idEstado >>> 32)));
		hash = hash * prime + ((int) (this.idAlarma ^ (this.idAlarma >>> 32)));
		
		return hash;
	}
}