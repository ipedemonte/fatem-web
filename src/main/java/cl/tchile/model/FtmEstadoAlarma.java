package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the FTM_ESTADO_ALARMA database table.
 * 
 */
@Entity
@Table(name="FTM_ESTADO_ALARMA")
@NamedQuery(name="FtmEstadoAlarma.findAll", query="SELECT f FROM FtmEstadoAlarma f")
public class FtmEstadoAlarma implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@Column(name="ID_ESTADO")
	private long idEstado;

	//@Id
	//@Column(name="ID_ALARMA")
	private long idAlarma;

	@Column(name="FECHA_ESTADO")
	private Timestamp fechaEstado;

	//bi-directional many-to-one association to FtmAlarma
	//@OneToMany(mappedBy="ftmEstadoAlarma")
	//private List<FtmAlarma> ftmAlarmas;

	public FtmEstadoAlarma() {
	}

	public Timestamp getFechaEstado() {
		return this.fechaEstado;
	}

	public void setFechaEstado(Timestamp fechaEstado) {
		this.fechaEstado = fechaEstado;
	}

//	public List<FtmAlarma> getFtmAlarmas() {
//		return this.ftmAlarmas;
//	}
//
//	public void setFtmAlarmas(List<FtmAlarma> ftmAlarmas) {
//		this.ftmAlarmas = ftmAlarmas;
//	}

//	public FtmAlarma addFtmAlarma(FtmAlarma ftmAlarma) {
//		getFtmAlarmas().add(ftmAlarma);
//		//ftmAlarma.setFtmEstadoAlarma(this);
//
//		return ftmAlarma;
//	}
//
//	public FtmAlarma removeFtmAlarma(FtmAlarma ftmAlarma) {
//		getFtmAlarmas().remove(ftmAlarma);
//		//ftmAlarma.setFtmEstadoAlarma(null);
//
//		return ftmAlarma;
//	}

}