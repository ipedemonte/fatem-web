package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FTM_ESTADO database table.
 * 
 */
@Entity
@Table(name="FTM_ESTADO")
@NamedQuery(name="FtmEstado.findAll", query="SELECT f FROM FtmEstado f")
public class FtmEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ESTADO")
	@JoinColumn(name="ID_ESTADO", table = "FTM_ESTADO_ALARMA")
	private long idEstado;

	private String nombre;

	public FtmEstado() {
	}

	public long getIdEstado() {
		return this.idEstado;
	}

	public void setIdEstado(long idEstado) {
		this.idEstado = idEstado;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}