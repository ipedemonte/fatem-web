package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the FTM_DETALLE_ALARMA database table.
 * 
 */
@Entity
@Table(name="FTM_DETALLE_ALARMA")
class FtmDetalleAlarma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_DETALLE_ALARMA")
	private long idDetalleAlarma;

	private String action;

	private String agenda;

	private String assignedGroup;

	private String assignedGroupShiftName;

	private String assignedSupportCompany;

	private String assignedSupportOrganization;

	private String assignee;

	private String bandejaFalla;

	@Column(name="CATEGORIZATION_TIER_1")
	private String categorizationTier1;

	@Column(name="CATEGORIZATION_TIER_2")
	private String categorizationTier2;

	@Column(name="CATEGORIZATION_TIER_3")
	private String categorizationTier3;

	private String ciname;

	private String clientesAltoValor;

	private String clientesEmpresa;

	private String clientesIptv;

	private String closureManofacturer;

	@Column(name="CLOSURE_PRODUCT_CATEGORY_TIER1")
	private String closureProductCategoryTier1;

	@Column(name="CLOSURE_PRODUCT_CATEGORY_TIER2")
	private String closureProductCategoryTier2;

	@Column(name="CLOSURE_PRODUCT_CATEGORY_TIER3")
	private String closureProductCategoryTier3;

	private String closureProductModelVersion;

	private String closureProductName;

	private BigDecimal codigoIvr;

	private String company;

	private String comunicationSource;

	private String createRequest;

	private String department;

	private String direccionInstalacion;

	private String directContactFirstName;

	private String directContactLastName;

	private String directContactMiddleInitial;

	private String firstName;

	private String impact;

	private String lastName;

	private String manufacturer;

	private String mensajeIvr;

	private String middleInitial;

	private String notes;

	private String password;

	private String priority;
	
	@Column(name="PRODUCT_CATEGORIZATION_TIER_1")
	private String productCategorizationTier1;

	@Column(name="PRODUCT_CATEGORIZATION_TIER_2")
	private String productCategorizationTier2;

	@Column(name="PRODUCT_CATEGORIZATION_TIER_3")
	private String productCategorizationTier3;

	private BigDecimal productModelVersion;

	private String productName;

	private String productosAfectados;

	private String razonSocial;

	private String reportedSource;

	private Timestamp requiredResolutionDatetime;

	private String rutCliente;

	private String serviceType;
	
	@Column(name="SERVICIO_R")
	private String servicioR;

	private String status;

	private String statusReason;

	private String summary;

	private String telefono;

	private String urgency;

	private String username;

	private String viewAccess;

	private Timestamp workInfoDate;

	private String workInfoLocked;

	private String workInfoNotes;

	private String workInfoSource;

	private String workInfoSummary;

	private String workInfoType;

	private String workInfoViewAccess;

	private String workinfoattachment1data;

	//bi-directional many-to-one association to FtmAlarma
	@ManyToOne
	@JoinColumn(name="ID_ALARMA")
	private FtmAlarma ftmAlarma;

	public FtmDetalleAlarma() {
	}

	public long getIdDetalleAlarma() {
		return this.idDetalleAlarma;
	}

	public void setIdDetalleAlarma(long idDetalleAlarma) {
		this.idDetalleAlarma = idDetalleAlarma;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAgenda() {
		return this.agenda;
	}

	public void setAgenda(String agenda) {
		this.agenda = agenda;
	}

	public String getAssignedGroup() {
		return this.assignedGroup;
	}

	public void setAssignedGroup(String assignedGroup) {
		this.assignedGroup = assignedGroup;
	}

	public String getAssignedGroupShiftName() {
		return this.assignedGroupShiftName;
	}

	public void setAssignedGroupShiftName(String assignedGroupShiftName) {
		this.assignedGroupShiftName = assignedGroupShiftName;
	}

	public String getAssignedSupportCompany() {
		return this.assignedSupportCompany;
	}

	public void setAssignedSupportCompany(String assignedSupportCompany) {
		this.assignedSupportCompany = assignedSupportCompany;
	}

	public String getAssignedSupportOrganization() {
		return this.assignedSupportOrganization;
	}

	public void setAssignedSupportOrganization(String assignedSupportOrganization) {
		this.assignedSupportOrganization = assignedSupportOrganization;
	}

	public String getAssignee() {
		return this.assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getBandejaFalla() {
		return this.bandejaFalla;
	}

	public void setBandejaFalla(String bandejaFalla) {
		this.bandejaFalla = bandejaFalla;
	}

	public String getCategorizationTier1() {
		return this.categorizationTier1;
	}

	public void setCategorizationTier1(String categorizationTier1) {
		this.categorizationTier1 = categorizationTier1;
	}

	public String getCategorizationTier2() {
		return this.categorizationTier2;
	}

	public void setCategorizationTier2(String categorizationTier2) {
		this.categorizationTier2 = categorizationTier2;
	}

	public String getCategorizationTier3() {
		return this.categorizationTier3;
	}

	public void setCategorizationTier3(String categorizationTier3) {
		this.categorizationTier3 = categorizationTier3;
	}

	public String getCiname() {
		return this.ciname;
	}

	public void setCiname(String ciname) {
		this.ciname = ciname;
	}

	public String getClientesAltoValor() {
		return this.clientesAltoValor;
	}

	public void setClientesAltoValor(String clientesAltoValor) {
		this.clientesAltoValor = clientesAltoValor;
	}

	public String getClientesEmpresa() {
		return this.clientesEmpresa;
	}

	public void setClientesEmpresa(String clientesEmpresa) {
		this.clientesEmpresa = clientesEmpresa;
	}

	public String getClientesIptv() {
		return this.clientesIptv;
	}

	public void setClientesIptv(String clientesIptv) {
		this.clientesIptv = clientesIptv;
	}

	public String getClosureManofacturer() {
		return this.closureManofacturer;
	}

	public void setClosureManofacturer(String closureManofacturer) {
		this.closureManofacturer = closureManofacturer;
	}

	public String getClosureProductCategoryTier1() {
		return this.closureProductCategoryTier1;
	}

	public void setClosureProductCategoryTier1(String closureProductCategoryTier1) {
		this.closureProductCategoryTier1 = closureProductCategoryTier1;
	}

	public String getClosureProductCategoryTier2() {
		return this.closureProductCategoryTier2;
	}

	public void setClosureProductCategoryTier2(String closureProductCategoryTier2) {
		this.closureProductCategoryTier2 = closureProductCategoryTier2;
	}

	public String getClosureProductCategoryTier3() {
		return this.closureProductCategoryTier3;
	}

	public void setClosureProductCategoryTier3(String closureProductCategoryTier3) {
		this.closureProductCategoryTier3 = closureProductCategoryTier3;
	}

	public String getClosureProductModelVersion() {
		return this.closureProductModelVersion;
	}

	public void setClosureProductModelVersion(String closureProductModelVersion) {
		this.closureProductModelVersion = closureProductModelVersion;
	}

	public String getClosureProductName() {
		return this.closureProductName;
	}

	public void setClosureProductName(String closureProductName) {
		this.closureProductName = closureProductName;
	}

	public BigDecimal getCodigoIvr() {
		return this.codigoIvr;
	}

	public void setCodigoIvr(BigDecimal codigoIvr) {
		this.codigoIvr = codigoIvr;
	}

	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComunicationSource() {
		return this.comunicationSource;
	}

	public void setComunicationSource(String comunicationSource) {
		this.comunicationSource = comunicationSource;
	}

	public String getCreateRequest() {
		return this.createRequest;
	}

	public void setCreateRequest(String createRequest) {
		this.createRequest = createRequest;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDireccionInstalacion() {
		return this.direccionInstalacion;
	}

	public void setDireccionInstalacion(String direccionInstalacion) {
		this.direccionInstalacion = direccionInstalacion;
	}

	public String getDirectContactFirstName() {
		return this.directContactFirstName;
	}

	public void setDirectContactFirstName(String directContactFirstName) {
		this.directContactFirstName = directContactFirstName;
	}

	public String getDirectContactLastName() {
		return this.directContactLastName;
	}

	public void setDirectContactLastName(String directContactLastName) {
		this.directContactLastName = directContactLastName;
	}

	public String getDirectContactMiddleInitial() {
		return this.directContactMiddleInitial;
	}

	public void setDirectContactMiddleInitial(String directContactMiddleInitial) {
		this.directContactMiddleInitial = directContactMiddleInitial;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImpact() {
		return this.impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getMensajeIvr() {
		return this.mensajeIvr;
	}

	public void setMensajeIvr(String mensajeIvr) {
		this.mensajeIvr = mensajeIvr;
	}

	public String getMiddleInitial() {
		return this.middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getProductCategorizationTier1() {
		return this.productCategorizationTier1;
	}

	public void setProductCategorizationTier1(String productCategorizationTier1) {
		this.productCategorizationTier1 = productCategorizationTier1;
	}

	public String getProductCategorizationTier2() {
		return this.productCategorizationTier2;
	}

	public void setProductCategorizationTier2(String productCategorizationTier2) {
		this.productCategorizationTier2 = productCategorizationTier2;
	}

	public String getProductCategorizationTier3() {
		return this.productCategorizationTier3;
	}

	public void setProductCategorizationTier3(String productCategorizationTier3) {
		this.productCategorizationTier3 = productCategorizationTier3;
	}

	public BigDecimal getProductModelVersion() {
		return this.productModelVersion;
	}

	public void setProductModelVersion(BigDecimal productModelVersion) {
		this.productModelVersion = productModelVersion;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductosAfectados() {
		return this.productosAfectados;
	}

	public void setProductosAfectados(String productosAfectados) {
		this.productosAfectados = productosAfectados;
	}

	public String getRazonSocial() {
		return this.razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getReportedSource() {
		return this.reportedSource;
	}

	public void setReportedSource(String reportedSource) {
		this.reportedSource = reportedSource;
	}

	public Timestamp getRequiredResolutionDatetime() {
		return this.requiredResolutionDatetime;
	}

	public void setRequiredResolutionDatetime(Timestamp requiredResolutionDatetime) {
		this.requiredResolutionDatetime = requiredResolutionDatetime;
	}

	public String getRutCliente() {
		return this.rutCliente;
	}

	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServicioR() {
		return this.servicioR;
	}

	public void setServicioR(String servicioR) {
		this.servicioR = servicioR;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusReason() {
		return this.statusReason;
	}

	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getUrgency() {
		return this.urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getViewAccess() {
		return this.viewAccess;
	}

	public void setViewAccess(String viewAccess) {
		this.viewAccess = viewAccess;
	}

	public Timestamp getWorkInfoDate() {
		return this.workInfoDate;
	}

	public void setWorkInfoDate(Timestamp workInfoDate) {
		this.workInfoDate = workInfoDate;
	}

	public String getWorkInfoLocked() {
		return this.workInfoLocked;
	}

	public void setWorkInfoLocked(String workInfoLocked) {
		this.workInfoLocked = workInfoLocked;
	}

	public String getWorkInfoNotes() {
		return this.workInfoNotes;
	}

	public void setWorkInfoNotes(String workInfoNotes) {
		this.workInfoNotes = workInfoNotes;
	}

	public String getWorkInfoSource() {
		return this.workInfoSource;
	}

	public void setWorkInfoSource(String workInfoSource) {
		this.workInfoSource = workInfoSource;
	}

	public String getWorkInfoSummary() {
		return this.workInfoSummary;
	}

	public void setWorkInfoSummary(String workInfoSummary) {
		this.workInfoSummary = workInfoSummary;
	}

	public String getWorkInfoType() {
		return this.workInfoType;
	}

	public void setWorkInfoType(String workInfoType) {
		this.workInfoType = workInfoType;
	}

	public String getWorkInfoViewAccess() {
		return this.workInfoViewAccess;
	}

	public void setWorkInfoViewAccess(String workInfoViewAccess) {
		this.workInfoViewAccess = workInfoViewAccess;
	}

	public String getWorkinfoattachment1data() {
		return this.workinfoattachment1data;
	}

	public void setWorkinfoattachment1data(String workinfoattachment1data) {
		this.workinfoattachment1data = workinfoattachment1data;
	}

	public FtmAlarma getFtmAlarma() {
		return this.ftmAlarma;
	}

	public void setFtmAlarma(FtmAlarma ftmAlarma) {
		this.ftmAlarma = ftmAlarma;
	}

}