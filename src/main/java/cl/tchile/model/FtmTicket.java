package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the FTM_TICKET database table.
 * 
 */
@Entity
@NamedQuery(name="FtmTicket.findAll", query="SELECT f FROM FtmTicket f")
public class FtmTicket implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long idTicket;

	private Timestamp fechaActualizacion;

	private Timestamp fechaCreacion;

	private String nombre;

	private String sistema;

	private String usuarioActualizacion;

	//bi-directional many-to-one association to FtmAlarma
	@OneToMany(mappedBy="ftmTicket")
	private List<FtmAlarma> ftmAlarmas;

	public FtmTicket() {
	}

	public long getIdTicket() {
		return this.idTicket;
	}

	public void setIdTicket(long idTicket) {
		this.idTicket = idTicket;
	}

	public Timestamp getFechaActualizacion() {
		return this.fechaActualizacion;
	}

	public void setFechaActualizacion(Timestamp fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSistema() {
		return this.sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public String getUsuarioActualizacion() {
		return this.usuarioActualizacion;
	}

	public void setUsuarioActualizacion(String usuarioActualizacion) {
		this.usuarioActualizacion = usuarioActualizacion;
	}

	public List<FtmAlarma> getFtmAlarmas() {
		return this.ftmAlarmas;
	}

	public void setFtmAlarmas(List<FtmAlarma> ftmAlarmas) {
		this.ftmAlarmas = ftmAlarmas;
	}

	public FtmAlarma addFtmAlarma(FtmAlarma ftmAlarma) {
		getFtmAlarmas().add(ftmAlarma);
		ftmAlarma.setFtmTicket(this);

		return ftmAlarma;
	}

	public FtmAlarma removeFtmAlarma(FtmAlarma ftmAlarma) {
		getFtmAlarmas().remove(ftmAlarma);
		ftmAlarma.setFtmTicket(null);

		return ftmAlarma;
	}

}