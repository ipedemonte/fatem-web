package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FTM_MODULO_PERFIL database table.
 * 
 */
@Entity
@Table(name="FTM_MODULO_PERFIL")
@NamedQuery(name="FtmModuloPerfil.findAll", query="SELECT f FROM FtmModuloPerfil f")
public class FtmModuloPerfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MODULO_PERFIL")
	private long idModuloPerfil;

	//bi-directional many-to-one association to FtmModulo
	@ManyToOne
	@JoinColumn(name="ID_MODULO")
	private FtmModulo ftmModulo;

	//bi-directional many-to-one association to FtmPerfil
	@ManyToOne
	@JoinColumn(name="ID_PERFIL")
	private FtmPerfil ftmPerfil;

	public FtmModuloPerfil() {
	}

	public long getIdModuloPerfil() {
		return this.idModuloPerfil;
	}

	public void setIdModuloPerfil(long idModuloPerfil) {
		this.idModuloPerfil = idModuloPerfil;
	}

	public FtmModulo getFtmModulo() {
		return this.ftmModulo;
	}

	public void setFtmModulo(FtmModulo ftmModulo) {
		this.ftmModulo = ftmModulo;
	}

	public FtmPerfil getFtmPerfil() {
		return this.ftmPerfil;
	}

	public void setFtmPerfil(FtmPerfil ftmPerfil) {
		this.ftmPerfil = ftmPerfil;
	}

}