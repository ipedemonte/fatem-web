package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the FTM_ALARMA database table.
 * 
 */
@Entity
@Table(name="FTM_ALARMA")
@NamedQueries(value = {
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersPta", query="SELECT f FROM FtmAlarma f where f.pta = ?1 "),
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersElemento", query="SELECT f FROM FtmAlarma f where f.naname = ?1 "),
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersZona", query="SELECT f FROM FtmAlarma f where f.nombreInterno = ?1 "),
        
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersZonaPta", query="SELECT f FROM FtmAlarma f where f.nombreInterno = ?1 and f.pta = ?2 "),
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersZonaElemnt", query="SELECT f FROM FtmAlarma f where f.nombreInterno = ?1 and f.naname = ?2 "),
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersPtaElement", query="SELECT f FROM FtmAlarma f where f.pta = ?1 and f.naname = ?2 "),
        @NamedQuery(name="FtmAlarma.getAlarmsFiltersZonaPtaElement", query="SELECT f FROM FtmAlarma f where f.nombreInterno = ?1 and f.pta = ?2 and f.naname = ?3 ")
        })

public class FtmAlarma implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_ALARMA")
	private long idAlarma;

	private String archivo;

	private Timestamp fechaFinEstimado;

	private Timestamp fechaInicio;

	private String naname;

	private String nombreInterno;

	private BigDecimal portpon;

	private String pta;

	private BigDecimal slot;

	private String tipoAlarma;

	//bi-directional many-to-one association to FtmEstadoAlarma
	//@ManyToOne
	//@JoinColumn(name="ID_ESTADO", referencedColumnName="ID_ESTADO", table = "FtmEstadoAlarma")
	//private FtmEstadoAlarma ftmEstadoAlarma;

	//bi-directional many-to-one association to FtmTicket
	@ManyToOne
	@JoinColumn(name="ID_TICKET")
	private FtmTicket ftmTicket;

	//bi-directional many-to-one association to FtmDetalleAlarma
	@OneToMany(mappedBy="ftmAlarma")
	private List<FtmDetalleAlarma> ftmDetalleAlarmas;

	//bi-directional many-to-one association to FtmTipoAcceso
	@ManyToOne
	@JoinColumn(name="ID_TIPO_ACCESO")
	private FtmTipoAcceso ftmTipoAcceso;

	public FtmAlarma() {
	}

	public long getIdAlarma() {
		return this.idAlarma;
	}

	public void setIdAlarma(long idAlarma) {
		this.idAlarma = idAlarma;
	}

	public String getArchivo() {
		return this.archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

	public Timestamp getFechaFinEstimado() {
		return this.fechaFinEstimado;
	}

	public void setFechaFinEstimado(Timestamp fechaFinEstimado) {
		this.fechaFinEstimado = fechaFinEstimado;
	}

	public Timestamp getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Timestamp fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getNaname() {
		return this.naname;
	}

	public void setNaname(String naname) {
		this.naname = naname;
	}

	public String getNombreInterno() {
		return this.nombreInterno;
	}

	public void setNombreInterno(String nombreInterno) {
		this.nombreInterno = nombreInterno;
	}

	public BigDecimal getPortpon() {
		return this.portpon;
	}

	public void setPortpon(BigDecimal portpon) {
		this.portpon = portpon;
	}

	public String getPta() {
		return this.pta;
	}

	public void setPta(String pta) {
		this.pta = pta;
	}

	public BigDecimal getSlot() {
		return this.slot;
	}

	public void setSlot(BigDecimal slot) {
		this.slot = slot;
	}

	public String getTipoAlarma() {
		return this.tipoAlarma;
	}

	public void setTipoAlarma(String tipoAlarma) {
		this.tipoAlarma = tipoAlarma;
	}

//	public FtmEstadoAlarma getFtmEstadoAlarma() {
//		return this.ftmEstadoAlarma;
//	}
//
//	public void setFtmEstadoAlarma(FtmEstadoAlarma ftmEstadoAlarma) {
//		this.ftmEstadoAlarma = ftmEstadoAlarma;
//	}

	public FtmTicket getFtmTicket() {
		return this.ftmTicket;
	}

	public void setFtmTicket(FtmTicket ftmTicket) {
		this.ftmTicket = ftmTicket;
	}

	public List<FtmDetalleAlarma> getFtmDetalleAlarmas() {
		return this.ftmDetalleAlarmas;
	}

	public void setFtmDetalleAlarmas(List<FtmDetalleAlarma> ftmDetalleAlarmas) {
		this.ftmDetalleAlarmas = ftmDetalleAlarmas;
	}

	public FtmDetalleAlarma addFtmDetalleAlarma(FtmDetalleAlarma ftmDetalleAlarma) {
		getFtmDetalleAlarmas().add(ftmDetalleAlarma);
		ftmDetalleAlarma.setFtmAlarma(this);

		return ftmDetalleAlarma;
	}

	public FtmDetalleAlarma removeFtmDetalleAlarma(FtmDetalleAlarma ftmDetalleAlarma) {
		getFtmDetalleAlarmas().remove(ftmDetalleAlarma);
		ftmDetalleAlarma.setFtmAlarma(null);

		return ftmDetalleAlarma;
	}

	public FtmTipoAcceso getFtmTipoAcceso() {
		return this.ftmTipoAcceso;
	}

	public void setFtmTipoAcceso(FtmTipoAcceso ftmTipoAcceso) {
		this.ftmTipoAcceso = ftmTipoAcceso;
	}

}