package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FTM_USUARIO database table.
 * 
 */
@Entity
@NamedQueries(value = {
		  @NamedQuery(name = "FtmUsuario.findUserAcces", query = "select u from FtmUsuario u where u.idUsuario = ?1") })
public class FtmUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long idUsuario;

	private String nombre;

	//bi-directional many-to-one association to FtmPerfil
	@ManyToOne
	@JoinColumn(name="ID_PERFIL")
	private FtmPerfil ftmPerfil;

	public FtmUsuario() {
	}

	public long getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public FtmPerfil getFtmPerfil() {
		return this.ftmPerfil;
	}

	public void setFtmPerfil(FtmPerfil ftmPerfil) {
		this.ftmPerfil = ftmPerfil;
	}

}