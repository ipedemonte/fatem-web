package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the FTM_TIPO_ACCESO database table.
 * 
 */
@Entity
@Table(name="FTM_TIPO_ACCESO")
@NamedQuery(name="FtmTipoAcceso.findAll", query="SELECT f FROM FtmTipoAcceso f")
public class FtmTipoAcceso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_TIPO_ACCESO")
	private long idTipoAcceso;

	private String nombre;

	//bi-directional many-to-one association to FtmAlarma
	@OneToMany(mappedBy="ftmTipoAcceso")
	private List<FtmAlarma> ftmAlarmas;

	public FtmTipoAcceso() {
	}

	public long getIdTipoAcceso() {
		return this.idTipoAcceso;
	}

	public void setIdTipoAcceso(long idTipoAcceso) {
		this.idTipoAcceso = idTipoAcceso;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<FtmAlarma> getFtmAlarmas() {
		return this.ftmAlarmas;
	}

	public void setFtmAlarmas(List<FtmAlarma> ftmAlarmas) {
		this.ftmAlarmas = ftmAlarmas;
	}

	public FtmAlarma addFtmAlarma(FtmAlarma ftmAlarma) {
		getFtmAlarmas().add(ftmAlarma);
		ftmAlarma.setFtmTipoAcceso(this);

		return ftmAlarma;
	}

	public FtmAlarma removeFtmAlarma(FtmAlarma ftmAlarma) {
		getFtmAlarmas().remove(ftmAlarma);
		ftmAlarma.setFtmTipoAcceso(null);

		return ftmAlarma;
	}

}