package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the FTM_PERFIL database table.
 * 
 */
@Entity
@Table(name="FTM_PERFIL")
@NamedQuery(name="FtmPerfil.findAll", query="SELECT f FROM FtmPerfil f")
public class FtmPerfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PERFIL")
	private long idPerfil;

	private String descripcion;

	private Timestamp fecha;

	private String nombre;

	//bi-directional many-to-one association to FtmModuloPerfil
	@OneToMany(mappedBy="ftmPerfil")
	private List<FtmModuloPerfil> ftmModuloPerfils;

	//bi-directional many-to-one association to FtmUsuario
	@OneToMany(mappedBy="ftmPerfil")
	private List<FtmUsuario> ftmUsuarios;

	public FtmPerfil() {
	}

	public long getIdPerfil() {
		return this.idPerfil;
	}

	public void setIdPerfil(long idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<FtmModuloPerfil> getFtmModuloPerfils() {
		return this.ftmModuloPerfils;
	}

	public void setFtmModuloPerfils(List<FtmModuloPerfil> ftmModuloPerfils) {
		this.ftmModuloPerfils = ftmModuloPerfils;
	}

	public FtmModuloPerfil addFtmModuloPerfil(FtmModuloPerfil ftmModuloPerfil) {
		getFtmModuloPerfils().add(ftmModuloPerfil);
		ftmModuloPerfil.setFtmPerfil(this);

		return ftmModuloPerfil;
	}

	public FtmModuloPerfil removeFtmModuloPerfil(FtmModuloPerfil ftmModuloPerfil) {
		getFtmModuloPerfils().remove(ftmModuloPerfil);
		ftmModuloPerfil.setFtmPerfil(null);

		return ftmModuloPerfil;
	}

	public List<FtmUsuario> getFtmUsuarios() {
		return this.ftmUsuarios;
	}

	public void setFtmUsuarios(List<FtmUsuario> ftmUsuarios) {
		this.ftmUsuarios = ftmUsuarios;
	}

	public FtmUsuario addFtmUsuario(FtmUsuario ftmUsuario) {
		getFtmUsuarios().add(ftmUsuario);
		ftmUsuario.setFtmPerfil(this);

		return ftmUsuario;
	}

	public FtmUsuario removeFtmUsuario(FtmUsuario ftmUsuario) {
		getFtmUsuarios().remove(ftmUsuario);
		ftmUsuario.setFtmPerfil(null);

		return ftmUsuario;
	}

}