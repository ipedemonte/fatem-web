package cl.tchile.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the FTM_MODULO database table.
 * 
 */
@Entity
@Table(name="FTM_MODULO")
@NamedQuery(name="FtmModulo.findAll", query="SELECT f FROM FtmModulo f")
public class FtmModulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_MODULO")
	private long idModulo;

	private String nombre;

	//bi-directional many-to-one association to FtmModuloPerfil
	@OneToMany(mappedBy="ftmModulo")
	private List<FtmModuloPerfil> ftmModuloPerfils;

	public FtmModulo() {
	}

	public long getIdModulo() {
		return this.idModulo;
	}

	public void setIdModulo(long idModulo) {
		this.idModulo = idModulo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<FtmModuloPerfil> getFtmModuloPerfils() {
		return this.ftmModuloPerfils;
	}

	public void setFtmModuloPerfils(List<FtmModuloPerfil> ftmModuloPerfils) {
		this.ftmModuloPerfils = ftmModuloPerfils;
	}

	public FtmModuloPerfil addFtmModuloPerfil(FtmModuloPerfil ftmModuloPerfil) {
		getFtmModuloPerfils().add(ftmModuloPerfil);
		ftmModuloPerfil.setFtmModulo(this);

		return ftmModuloPerfil;
	}

	public FtmModuloPerfil removeFtmModuloPerfil(FtmModuloPerfil ftmModuloPerfil) {
		getFtmModuloPerfils().remove(ftmModuloPerfil);
		ftmModuloPerfil.setFtmModulo(null);

		return ftmModuloPerfil;
	}

}