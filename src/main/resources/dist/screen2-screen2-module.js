(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["screen2-screen2-module"],{

/***/ "./src/app/layout/screen2/screen2-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/screen2/screen2-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: Screen2RoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen2RoutingModule", function() { return Screen2RoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _screen2_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./screen2.component */ "./src/app/layout/screen2/screen2.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _screen2_component__WEBPACK_IMPORTED_MODULE_2__["Screen2Component"]
    }
];
var Screen2RoutingModule = /** @class */ (function () {
    function Screen2RoutingModule() {
    }
    Screen2RoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], Screen2RoutingModule);
    return Screen2RoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/screen2/screen2.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/screen2/screen2.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"mat-elevation-z8\">  \r\n<div class=\"mb-20\"  flFlex flexLayout=\"row\" flexLayout.lt-md=\"column\">\r\n    <div fxFlex>\r\n        <table width=\"115px\" mat-table [dataSource]=\"dataSource\" >\r\n            <ng-container matColumnDef=\"position\">\r\n                <th mat-header-cell *matHeaderCellDef> No. </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.position}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"name\">\r\n                <th mat-header-cell *matHeaderCellDef> Name </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.name}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"weight\">\r\n                <th mat-header-cell *matHeaderCellDef> Weight </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.weight}} </td>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"symbol\">\r\n                <th mat-header-cell *matHeaderCellDef> Symbol </th>\r\n                <td mat-cell *matCellDef=\"let element\"> {{element.symbol}} </td>\r\n            </ng-container>\r\n            <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n        </table>\r\n        <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator> \r\n    </div>\r\n</div>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/screen2/screen2.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/screen2/screen2.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%; }\n\n.mat-card {\n  text-align: center; }\n\n.mat-card img {\n    border-radius: 5px;\n    margin-top: -25px; }\n\n.mat-table {\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z8 {\n  padding-top: 15%; }\n\n.mb-20 {\n  margin-bottom: 20px;\n  padding-top: 15%;\n  width: 100%; }\n\n#tabla {\n  background: #003245; }\n\n.mat-elevation-z8 {\n  padding: 2%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L3NjcmVlbjIvQzpcXFVzZXJzXFxjc2Flem1cXERlc2t0b3BcXG51ZXZvL3NyY1xcYXBwXFxsYXlvdXRcXHNjcmVlbjJcXHNjcmVlbjIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXLEVBQ2Q7O0FBQ0Q7RUFDSSxtQkFBa0IsRUFLckI7O0FBTkQ7SUFHUSxtQkFBa0I7SUFDbEIsa0JBQWlCLEVBQ3BCOztBQUVMO0VBQ0ksZ0hBQ21DLEVBQ3RDOztBQUVEO0VBQ0ksaUJBQWdCLEVBRW5COztBQUNEO0VBQ0ksb0JBQW1CO0VBQ25CLGlCQUFnQjtFQUNoQixZQUFXLEVBQ2Q7O0FBRUQ7RUFDSSxvQkFBbUIsRUFDdEI7O0FBQ0Q7RUFDQSxZQUFXLEVBQ1YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvc2NyZWVuMi9zY3JlZW4yLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLm1hdC1jYXJkIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGltZyB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IC0yNXB4O1xyXG4gICAgfVxyXG59XHJcbi5tYXQtdGFibGUge1xyXG4gICAgYm94LXNoYWRvdzogMCAzcHggMXB4IC0ycHggcmdiYSgwLCAwLCAwLCAwLjIpLCAwIDJweCAycHggMCByZ2JhKDAsIDAsIDAsIDAuMTQpLFxyXG4gICAgICAgIDAgMXB4IDVweCAwIHJnYmEoMCwgMCwgMCwgMC4xMik7XHJcbn1cclxuXHJcbi5tYXQtZWxldmF0aW9uLXo4e1xyXG4gICAgcGFkZGluZy10b3A6IDE1JTtcclxuXHJcbn1cclxuLm1iLTIwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTUlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbiN0YWJsYXtcclxuICAgIGJhY2tncm91bmQ6ICMwMDMyNDU7XHJcbn1cclxuLm1hdC1lbGV2YXRpb24tejh7XHJcbnBhZGRpbmc6IDIlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/layout/screen2/screen2.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/screen2/screen2.component.ts ***!
  \*****************************************************/
/*! exports provided: Screen2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen2Component", function() { return Screen2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ELEMENT_DATA = [
    { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
    { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
    { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
    { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
    { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
    { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
    { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' }
];
var Screen2Component = /** @class */ (function () {
    function Screen2Component() {
        this.displayedColumns = ['position', 'name', 'weight', 'symbol'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
        this.places = [];
        this.places = [
            {
                imgSrc: 'assets/images/card-1.jpg',
                place: 'Cozy 5 Stars Apartment',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
                charge: '$899/night',
                location: 'Barcelona, Spain'
            },
            {
                imgSrc: 'assets/images/card-2.jpg',
                place: 'Office Studio',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in London, UK.',
                charge: '$1,119/night',
                location: 'London, UK'
            },
            {
                imgSrc: 'assets/images/card-3.jpg',
                place: 'Beautiful Castle',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Milan.',
                charge: '$459/night',
                location: 'Milan, Italy'
            }
        ];
    }
    Screen2Component.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    Screen2Component.prototype.ngOnInit = function () { };
    Screen2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-screen2',
            template: __webpack_require__(/*! ./screen2.component.html */ "./src/app/layout/screen2/screen2.component.html"),
            styles: [__webpack_require__(/*! ./screen2.component.scss */ "./src/app/layout/screen2/screen2.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Screen2Component);
    return Screen2Component;
}());



/***/ }),

/***/ "./src/app/layout/screen2/screen2.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/screen2/screen2.module.ts ***!
  \**************************************************/
/*! exports provided: Screen2Module */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen2Module", function() { return Screen2Module; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _screen2_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./screen2-routing.module */ "./src/app/layout/screen2/screen2-routing.module.ts");
/* harmony import */ var _screen2_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./screen2.component */ "./src/app/layout/screen2/screen2.component.ts");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var Screen2Module = /** @class */ (function () {
    function Screen2Module() {
    }
    Screen2Module = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _screen2_routing_module__WEBPACK_IMPORTED_MODULE_6__["Screen2RoutingModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__["StatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
            ],
            declarations: [_screen2_component__WEBPACK_IMPORTED_MODULE_7__["Screen2Component"]]
        })
    ], Screen2Module);
    return Screen2Module;
}());



/***/ })

}]);
//# sourceMappingURL=screen2-screen2-module.js.map