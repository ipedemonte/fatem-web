(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "./src/app/layout/components/sidebar/sidebar.component.html":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form  class=\"login-form\" fxFlex  >\r\n        <div class= \"col-lg-8\" id=\"panelPrin\"  >\r\n            <h1 id=\"letra\">HISTORIAL</h1>\r\n            <hr class=\"my-4\">\r\n        </div>    \r\n        <div class=\"row\">\r\n        <div class= \"col-sm-4\" id=\"panel\"    >\r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput [(ngModel)]=\"fecha_inicio\" placeholder=\"Fecha Inicio\"  name=\"fecha_inicio\" >\r\n            </mat-form-field>\r\n        </div>\r\n        <div class= \"col-sm-4\" id=\"panel\"    >    \r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput  [(ngModel)]=\"fecha_fin\"  placeholder=\"Fecha Fin\" name=\"fecha_fin\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div id = \"Consultar\" class= \"col-sm-8\"    >\r\n            <button id = \"boton\" mat-raised-button color=\"primary\"  (click)=\"onLogin()\">\r\n                    Consultar\r\n            </button>\r\n        </div>  \r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: #003245;\n  height: 100%;\n  position: relative;\n  text-align: center; }\n  .login-page .content {\n    z-index: 1;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n  .login-page .content .login-form {\n      padding: 40px;\n      background: #fff;\n      width: 500px;\n      box-shadow: 0 0 10px #ddd; }\n  .login-page .content .login-form input:-webkit-autofill {\n        -webkit-box-shadow: 0 0 0 30px white inset; }\n  .login-page input {\n    color: #003245; }\n  #letra {\n  color: #003245;\n  font-size: 40px;\n  margin-top: 0px;\n  padding-bottom: 10px; }\n  #letra2 {\n  color: white;\n  font-size: 20px;\n  margin-top: 0px;\n  padding-bottom: 10px;\n  align-self: auto; }\n  #boton {\n  background: #003245;\n  width: 30%; }\n  .img-thumbnail {\n  border: 600px; }\n  .img-telef {\n  margin: 10px; }\n  .example-full-width {\n  width: 100px; }\n  #panel {\n  padding-top: 4%;\n  text-align: center;\n  align-content: center; }\n  #panelPrin {\n  padding-top: 8%; }\n  #Consultar {\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvc2lkZWJhci9DOlxcVXNlcnNcXGNzYWV6bVxcRGVza3RvcFxcbnVldm8vc3JjXFxhcHBcXGxheW91dFxcY29tcG9uZW50c1xcc2lkZWJhclxcc2lkZWJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQWE7RUFDYixvQkFBbUI7RUFDbkIsd0JBQXVCO0VBQ3ZCLG9CQUFtQjtFQUNuQixhQUFZO0VBQ1osbUJBQWtCO0VBQ2xCLG1CQUFrQixFQXNCckI7RUE3QkQ7SUFVUSxXQUFVO0lBQ1YsY0FBYTtJQUNiLG9CQUFtQjtJQUNuQix3QkFBdUIsRUFVMUI7RUF2Qkw7TUFlWSxjQUFhO01BQ2IsaUJBQWdCO01BQ2hCLGFBQVk7TUFDWiwwQkFBeUIsRUFJNUI7RUF0QlQ7UUFvQmdCLDJDQUEwQyxFQUM3QztFQXJCYjtJQTJCUSxlQUFjLEVBQ2pCO0VBR0g7RUFDSSxlQUFjO0VBQ2QsZ0JBQWM7RUFDZCxnQkFBZTtFQUNmLHFCQUFvQixFQUN2QjtFQUNEO0VBQ0UsYUFBYTtFQUNiLGdCQUFjO0VBQ2QsZ0JBQWU7RUFDZixxQkFBb0I7RUFDcEIsaUJBQWdCLEVBQ25CO0VBQ0M7RUFDSSxvQkFBbUI7RUFDbkIsV0FBVSxFQUNiO0VBQ0Q7RUFDRSxjQUFhLEVBRWQ7RUFDSDtFQUNJLGFBQVksRUFDZjtFQUNDO0VBQ0ksYUFBWSxFQUdmO0VBQ0Q7RUFDUSxnQkFBZTtFQUNmLG1CQUFrQjtFQUNsQixzQkFBcUIsRUFFNUI7RUFDRDtFQUNFLGdCQUFlLEVBRWhCO0VBQ0Q7RUFDRyxtQkFBa0IsRUFDcEIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9zaWRlYmFyL3NpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9naW4tcGFnZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZDogIzAwMzI0NTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAuY29udGVudCB7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgLmxvZ2luLWZvcm0ge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICB3aWR0aDogNTAwcHg7XHJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAxMHB4ICNkZGQ7XHJcbiAgICAgICAgICAgIGlucHV0Oi13ZWJraXQtYXV0b2ZpbGwge1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMCAzMHB4IHdoaXRlIGluc2V0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgICBcclxuICAgIGlucHV0e1xyXG4gICAgICAgIGNvbG9yOiAjMDAzMjQ1O1xyXG4gICAgfVxyXG59XHJcblxyXG4gICNsZXRyYXtcclxuICAgICAgY29sb3I6ICMwMDMyNDU7XHJcbiAgICAgIGZvbnQtc2l6ZTo0MHB4O1x0XHJcbiAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgfVxyXG4gICNsZXRyYTJ7XHJcbiAgICBjb2xvcjogIHdoaXRlO1xyXG4gICAgZm9udC1zaXplOjIwcHg7XHRcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xyXG4gICAgYWxpZ24tc2VsZjogYXV0bztcclxufVxyXG4gICNib3RvbntcclxuICAgICAgYmFja2dyb3VuZDogIzAwMzI0NTtcclxuICAgICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbiAgLmltZy10aHVtYm5haWx7XHJcbiAgICBib3JkZXI6NjAwcHggO1xyXG4gIFxyXG4gIH1cclxuLmltZy10ZWxlZntcclxuICAgIG1hcmdpbjogMTBweDtcclxufVxyXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGh7XHJcbiAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgXHJcblxyXG4gIH1cclxuICAjcGFuZWx7XHJcbiAgICAgICAgICBwYWRkaW5nLXRvcDogNCU7IFxyXG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG5cclxuICB9XHJcbiAgI3BhbmVsUHJpbntcclxuICAgIHBhZGRpbmctdG9wOiA4JTsgXHJcblxyXG4gIH1cclxuICAjQ29uc3VsdGFye1xyXG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/layout/components/sidebar/sidebar.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/layout/components/sidebar/sidebar.component.ts ***!
  \****************************************************************/
/*! exports provided: SidebarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarComponent", function() { return SidebarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SidebarComponent = /** @class */ (function () {
    function SidebarComponent() {
        this.foods = [
            { value: 'FO', viewValue: 'FO' },
            { value: 'CU', viewValue: 'CU' }
        ];
    }
    SidebarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/layout/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/layout/components/sidebar/sidebar.component.scss")]
        })
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.html":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"mat-elevation-z8\">  \r\n        <app-sidebar></app-sidebar>\r\n        <div class=\"mb-20\"  flFlex flexLayout=\"row\" flexLayout.lt-md=\"column\">\r\n            <div fxFlex>\r\n                <table width=\"115px\" mat-table [dataSource]=\"dataSource\" >\r\n                    <ng-container matColumnDef=\"elemento\">\r\n                        <th mat-header-cell *matHeaderCellDef> Elemento </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.elemento}} </td>\r\n                    </ng-container>\r\n                    <ng-container matColumnDef=\"planta\">\r\n                        <th mat-header-cell *matHeaderCellDef> Planta </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.planta}} </td>\r\n                    </ng-container>\r\n                    <ng-container matColumnDef=\"hostname\">\r\n                        <th mat-header-cell *matHeaderCellDef> Hostname </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.hostname}} </td>\r\n                    </ng-container>\r\n                    <ng-container matColumnDef=\"summary\">\r\n                            <th mat-header-cell *matHeaderCellDef> Summary </th>\r\n                            <td mat-cell *matCellDef=\"let element\"> {{element.summary}} </td>\r\n                    </ng-container>\r\n                    <ng-container matColumnDef=\"zona\">\r\n                        <th mat-header-cell *matHeaderCellDef> Zona </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.zona}} </td>\r\n                    </ng-container>\r\n                    \r\n                    <ng-container matColumnDef=\"cantidad\">\r\n                                <th mat-header-cell *matHeaderCellDef> Cantidad </th>\r\n                                <td mat-cell *matCellDef=\"let element\"> {{element.cantidad}} </td>\r\n                    </ng-container>    \r\n                    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n                    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n\r\n                </table>\r\n            </div>\r\n            <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator> \r\n\r\n        </div>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%; }\n\n.mat-card {\n  text-align: center; }\n\n.mat-card img {\n    border-radius: 5px;\n    margin-top: -25px; }\n\n.mat-table {\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z8 {\n  padding-top: 15%;\n  background: #EDEBEA;\n  width: 100%; }\n\n.mb-20 {\n  margin-bottom: 20px;\n  padding-top: 5%;\n  width: 100%; }\n\n#tabla {\n  background: #003245; }\n\n.mat-elevation-z8 {\n  padding: 2%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2Rhc2hib2FyZC9DOlxcVXNlcnNcXGNzYWV6bVxcRGVza3RvcFxcbnVldm8vc3JjXFxhcHBcXGxheW91dFxcZGFzaGJvYXJkXFxkYXNoYm9hcmQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXLEVBQ2Q7O0FBQ0Q7RUFDSSxtQkFBa0IsRUFLckI7O0FBTkQ7SUFHUSxtQkFBa0I7SUFDbEIsa0JBQWlCLEVBQ3BCOztBQUVMO0VBQ0ksZ0hBQ21DLEVBQ3RDOztBQUVEO0VBQ0ksaUJBQWdCO0VBQ2hCLG9CQUFrQjtFQUNsQixZQUFXLEVBRWQ7O0FBQ0Q7RUFDSSxvQkFBbUI7RUFDbkIsZ0JBQWU7RUFDZixZQUFXLEVBQ2Q7O0FBR0Q7RUFDSSxvQkFBbUIsRUFDdEI7O0FBQ0Q7RUFDQSxZQUFXLEVBQ1YiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5tYXQtY2FyZCB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMjVweDtcclxuICAgIH1cclxufVxyXG4ubWF0LXRhYmxlIHtcclxuICAgIGJveC1zaGFkb3c6IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSxcclxuICAgICAgICAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OHtcclxuICAgIHBhZGRpbmctdG9wOiAxNSU7XHJcbiAgICBiYWNrZ3JvdW5kOiNFREVCRUE7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuXHJcbn1cclxuLm1iLTIwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogNSU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuXHJcbiN0YWJsYXtcclxuICAgIGJhY2tncm91bmQ6ICMwMDMyNDU7XHJcbn1cclxuLm1hdC1lbGV2YXRpb24tejh7XHJcbnBhZGRpbmc6IDIlO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.component.ts ***!
  \*********************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ELEMENT_DATA = [
    { elemento: 'PROD_', planta: 1, hostname: 'host', summary: 'prueba', zona: "Santiago", cantidad: 1 },
    { elemento: 'PROD_', planta: 2, hostname: 'host', summary: 'prueba', zona: "Santiago", cantidad: 2 },
    { elemento: 'PROD_', planta: 3, hostname: 'host', summary: 'prueba', zona: "Santiago", cantidad: 3 },
    { elemento: 'PROD_', planta: 4, hostname: 'host', summary: 'prueba', zona: "Santiago", cantidad: 1 },
    { elemento: 'PROD_', planta: 5, hostname: 'host', summary: 'prueba', zona: "Santiago", cantidad: 2 }
];
var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
        this.displayedColumns = ['elemento', 'planta', 'hostname', 'summary', 'zona', 'cantidad'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
        this.places = [];
        this.places = [
            {
                imgSrc: 'assets/images/card-1.jpg',
                place: 'Cozy 5 Stars Apartment',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
                charge: '$899/night',
                location: 'Barcelona, Spain'
            },
            {
                imgSrc: 'assets/images/card-2.jpg',
                place: 'Office Studio',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in London, UK.',
                charge: '$1,119/night',
                location: 'London, UK'
            },
            {
                imgSrc: 'assets/images/card-3.jpg',
                place: 'Beautiful Castle',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Milan.',
                charge: '$459/night',
                location: 'Milan, Italy'
            }
        ];
    }
    DashboardComponent.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    DashboardComponent.prototype.ngOnInit = function () { };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/layout/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/layout/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/layout/dashboard/dashboard.module.ts":
/*!******************************************************!*\
  !*** ./src/app/layout/dashboard/dashboard.module.ts ***!
  \******************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/layout/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/layout/dashboard/dashboard.component.ts");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/sidebar/sidebar.component */ "./src/app/layout/components/sidebar/sidebar.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__["DashboardRoutingModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__["StatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_6__["DashboardRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
            ],
            declarations: [_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"], _components_sidebar_sidebar_component__WEBPACK_IMPORTED_MODULE_11__["SidebarComponent"]]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map