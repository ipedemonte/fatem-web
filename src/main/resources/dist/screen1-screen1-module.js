(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["screen1-screen1-module"],{

/***/ "./src/app/layout/components/consultar2/sidebar.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/layout/components/consultar2/sidebar.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form  class=\"login-form\" fxFlex  >\r\n        <div class= \"col-lg-8\" id=\"panelPrin\"  >\r\n            <h1 id=\"letra\">ALARMAS RE-ACTIVAS</h1>\r\n            <hr class=\"my-4\">\r\n        </div>    \r\n        <div class=\"row\">\r\n        <div class= \"col-sm-2\" id=\"panel\"    >\r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput [(ngModel)]=\"slot\" placeholder=\"Slot\"  name=\"slot\" >\r\n            </mat-form-field>\r\n        </div>\r\n        <div class= \"col-sm-2\" id=\"panel\"    >    \r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput  [(ngModel)]=\"agencia\"  placeholder=\"Agencia\" name=\"agencia\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class= \"col-sm-2\" id=\"panel\"    >\r\n            <mat-form-field class=\"example-full-width\">\r\n                <input matInput  [(ngModel)]=\"summary\"  placeholder=\"Summary\" name=\"summary\">\r\n            </mat-form-field>\r\n        </div>\r\n        <div class= \"col-sm-2\" id=\"panel\"    >\r\n            <mat-form-field>\r\n                <mat-select placeholder=\"Type Tecnology\">\r\n                    <mat-option *ngFor=\"let food of foods\" [value]=\"food.value\">\r\n                        {{food.viewValue}}\r\n                    </mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </div>      \r\n        <div class= \"col-sm-8\"    >\r\n            <button id = \"boton\" mat-raised-button color=\"primary\"  (click)=\"onLogin()\">\r\n                    Consultar\r\n            </button>\r\n        </div>  \r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./src/app/layout/components/consultar2/sidebar.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/layout/components/consultar2/sidebar.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-page {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: #003245;\n  height: 100%;\n  position: relative;\n  text-align: center; }\n  .login-page .content {\n    z-index: 1;\n    display: flex;\n    align-items: center;\n    justify-content: center; }\n  .login-page .content .login-form {\n      padding: 40px;\n      background: #fff;\n      width: 500px;\n      box-shadow: 0 0 10px #ddd; }\n  .login-page .content .login-form input:-webkit-autofill {\n        -webkit-box-shadow: 0 0 0 30px white inset; }\n  #letra {\n  color: #003245;\n  font-size: 40px;\n  margin-top: 0px;\n  padding-bottom: 10px; }\n  #letra2 {\n  color: white;\n  font-size: 20px;\n  margin-top: 0px;\n  padding-bottom: 10px;\n  align-self: auto; }\n  #boton {\n  background: #003245;\n  width: 30%;\n  text-align: center; }\n  .img-thumbnail {\n  border: 600px; }\n  .img-telef {\n  margin: 10px; }\n  .example-full-width {\n  width: 100px; }\n  #panel {\n  padding-top: 4%;\n  text-align: center;\n  align-content: center; }\n  #panelPrin {\n  padding-top: 8%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L2NvbXBvbmVudHMvY29uc3VsdGFyMi9DOlxcVXNlcnNcXGNzYWV6bVxcRGVza3RvcFxcbnVldm8vc3JjXFxhcHBcXGxheW91dFxcY29tcG9uZW50c1xcY29uc3VsdGFyMlxcc2lkZWJhci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQWE7RUFDYixvQkFBbUI7RUFDbkIsd0JBQXVCO0VBQ3ZCLG9CQUFtQjtFQUNuQixhQUFZO0VBQ1osbUJBQWtCO0VBQ2xCLG1CQUFrQixFQW1CckI7RUExQkQ7SUFVUSxXQUFVO0lBQ1YsY0FBYTtJQUNiLG9CQUFtQjtJQUNuQix3QkFBdUIsRUFVMUI7RUF2Qkw7TUFlWSxjQUFhO01BQ2IsaUJBQWdCO01BQ2hCLGFBQVk7TUFDWiwwQkFBeUIsRUFJNUI7RUF0QlQ7UUFvQmdCLDJDQUEwQyxFQUM3QztFQU1YO0VBQ0ksZUFBYztFQUNkLGdCQUFjO0VBQ2QsZ0JBQWU7RUFDZixxQkFBb0IsRUFDdkI7RUFDRDtFQUNFLGFBQWE7RUFDYixnQkFBYztFQUNkLGdCQUFlO0VBQ2YscUJBQW9CO0VBQ3BCLGlCQUFnQixFQUNuQjtFQUNDO0VBQ0ksb0JBQW1CO0VBQ25CLFdBQVU7RUFDVixtQkFBa0IsRUFDckI7RUFDRDtFQUNFLGNBQWEsRUFFZDtFQUNIO0VBQ0ksYUFBWSxFQUNmO0VBQ0M7RUFDSSxhQUFZLEVBR2Y7RUFDRDtFQUNRLGdCQUFlO0VBQ2YsbUJBQWtCO0VBQ2xCLHNCQUFxQixFQUU1QjtFQUNEO0VBQ0UsZ0JBQWUsRUFFaEIiLCJmaWxlIjoic3JjL2FwcC9sYXlvdXQvY29tcG9uZW50cy9jb25zdWx0YXIyL3NpZGViYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9naW4tcGFnZSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZDogIzAwMzI0NTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICAuY29udGVudCB7XHJcbiAgICAgICAgei1pbmRleDogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgLmxvZ2luLWZvcm0ge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgICAgICB3aWR0aDogNTAwcHg7XHJcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMCAxMHB4ICNkZGQ7XHJcbiAgICAgICAgICAgIGlucHV0Oi13ZWJraXQtYXV0b2ZpbGwge1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDAgMCAzMHB4IHdoaXRlIGluc2V0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFxyXG59XHJcbiAgI2xldHJhe1xyXG4gICAgICBjb2xvcjogIzAwMzI0NTtcclxuICAgICAgZm9udC1zaXplOjQwcHg7XHRcclxuICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxuICB9XHJcbiAgI2xldHJhMntcclxuICAgIGNvbG9yOiAgd2hpdGU7XHJcbiAgICBmb250LXNpemU6MjBweDtcdFxyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbiAgICBhbGlnbi1zZWxmOiBhdXRvO1xyXG59XHJcbiAgI2JvdG9ue1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjMDAzMjQ1O1xyXG4gICAgICB3aWR0aDogMzAlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5pbWctdGh1bWJuYWlse1xyXG4gICAgYm9yZGVyOjYwMHB4IDtcclxuICBcclxuICB9XHJcbi5pbWctdGVsZWZ7XHJcbiAgICBtYXJnaW46IDEwcHg7XHJcbn1cclxuICAuZXhhbXBsZS1mdWxsLXdpZHRoe1xyXG4gICAgICB3aWR0aDogMTAwcHg7XHJcbiAgICAgIFxyXG5cclxuICB9XHJcbiAgI3BhbmVse1xyXG4gICAgICAgICAgcGFkZGluZy10b3A6IDQlOyBcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgfVxyXG4gICNwYW5lbFByaW57XHJcbiAgICBwYWRkaW5nLXRvcDogOCU7IFxyXG5cclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/layout/components/consultar2/sidebar.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/layout/components/consultar2/sidebar.component.ts ***!
  \*******************************************************************/
/*! exports provided: Consultar2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Consultar2", function() { return Consultar2; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Consultar2 = /** @class */ (function () {
    function Consultar2() {
        this.foods = [
            { value: 'FO', viewValue: 'FO' },
            { value: 'CU', viewValue: 'CU' }
        ];
    }
    Consultar2 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar',
            template: __webpack_require__(/*! ./sidebar.component.html */ "./src/app/layout/components/consultar2/sidebar.component.html"),
            styles: [__webpack_require__(/*! ./sidebar.component.scss */ "./src/app/layout/components/consultar2/sidebar.component.scss")]
        })
    ], Consultar2);
    return Consultar2;
}());



/***/ }),

/***/ "./src/app/layout/screen1/screen1-routing.module.ts":
/*!**********************************************************!*\
  !*** ./src/app/layout/screen1/screen1-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: Screen1RoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1RoutingModule", function() { return Screen1RoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _screen1_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./screen1.component */ "./src/app/layout/screen1/screen1.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _screen1_component__WEBPACK_IMPORTED_MODULE_2__["Screen1Component"]
    }
];
var Screen1RoutingModule = /** @class */ (function () {
    function Screen1RoutingModule() {
    }
    Screen1RoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], Screen1RoutingModule);
    return Screen1RoutingModule;
}());



/***/ }),

/***/ "./src/app/layout/screen1/screen1.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/screen1/screen1.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"mat-elevation-z8\">  \r\n    <app-sidebar></app-sidebar>\r\n    <div class=\"mb-20\"  flFlex flexLayout=\"row\" flexLayout.lt-md=\"column\">\r\n        <div fxFlex>\r\n            <table width=\"115px\" mat-table [dataSource]=\"dataSource\" >\r\n                <ng-container matColumnDef=\"fechainicio\">\r\n                    <th mat-header-cell *matHeaderCellDef> Fecha_Inicio </th>\r\n                    <td mat-cell *matCellDef=\"let element\"> {{element.fechaIni}} </td>\r\n                </ng-container>\r\n                <ng-container matColumnDef=\"id_alarma\">\r\n                    <th mat-header-cell *matHeaderCellDef> Id_Alarma </th>\r\n                    <td mat-cell *matCellDef=\"let element\"> {{element.idalarma}} </td>\r\n                </ng-container>\r\n                <ng-container matColumnDef=\"slot\">\r\n                    <th mat-header-cell *matHeaderCellDef> Slot </th>\r\n                    <td mat-cell *matCellDef=\"let element\"> {{element.slot}} </td>\r\n                </ng-container>\r\n                <ng-container matColumnDef=\"cod_ivr\">\r\n                    <th mat-header-cell *matHeaderCellDef> Cod_IVR </th>\r\n                    <td mat-cell *matCellDef=\"let element\"> {{element.codivr}} </td>\r\n                </ng-container>\r\n                <ng-container matColumnDef=\"summary\">\r\n                        <th mat-header-cell *matHeaderCellDef> Summary </th>\r\n                        <td mat-cell *matCellDef=\"let element\"> {{element.summary}} </td>\r\n                </ng-container>\r\n                <ng-container matColumnDef=\"totalclientes\">\r\n                            <th mat-header-cell *matHeaderCellDef> Total_Clientes </th>\r\n                            <td mat-cell *matCellDef=\"let element\"> {{element.totalclientes}} </td>\r\n                </ng-container>    \r\n                <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n                <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n            </table>\r\n            <mat-paginator [pageSizeOptions]=\"[5, 10, 20]\" showFirstLastButtons></mat-paginator> \r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/layout/screen1/screen1.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/layout/screen1/screen1.component.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table {\n  width: 100%; }\n\n.mat-card {\n  text-align: center; }\n\n.mat-card img {\n    border-radius: 5px;\n    margin-top: -25px; }\n\n.mat-table {\n  box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.mat-elevation-z8 {\n  padding-top: 15%; }\n\n.mb-20 {\n  margin-bottom: 20px;\n  padding-top: 5%;\n  width: 100%; }\n\n#tabla {\n  background: #003245; }\n\n.mat-elevation-z8 {\n  padding: 2%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L3NjcmVlbjEvQzpcXFVzZXJzXFxjc2Flem1cXERlc2t0b3BcXG51ZXZvL3NyY1xcYXBwXFxsYXlvdXRcXHNjcmVlbjFcXHNjcmVlbjEuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXLEVBQ2Q7O0FBQ0Q7RUFDSSxtQkFBa0IsRUFLckI7O0FBTkQ7SUFHUSxtQkFBa0I7SUFDbEIsa0JBQWlCLEVBQ3BCOztBQUVMO0VBQ0ksZ0hBQ21DLEVBQ3RDOztBQUVEO0VBQ0ksaUJBQWdCLEVBRW5COztBQUNEO0VBQ0ksb0JBQW1CO0VBQ25CLGdCQUFlO0VBQ2YsWUFBVyxFQUNkOztBQUdEO0VBQ0ksb0JBQW1CLEVBQ3RCOztBQUNEO0VBQ0EsWUFBVyxFQUNWIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L3NjcmVlbjEvc2NyZWVuMS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5tYXQtY2FyZCB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBpbWcge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAtMjVweDtcclxuICAgIH1cclxufVxyXG4ubWF0LXRhYmxlIHtcclxuICAgIGJveC1zaGFkb3c6IDAgM3B4IDFweCAtMnB4IHJnYmEoMCwgMCwgMCwgMC4yKSwgMCAycHggMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjE0KSxcclxuICAgICAgICAwIDFweCA1cHggMCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xyXG59XHJcblxyXG4ubWF0LWVsZXZhdGlvbi16OHtcclxuICAgIHBhZGRpbmctdG9wOiAxNSU7XHJcblxyXG59XHJcbi5tYi0yMCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDUlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcblxyXG4jdGFibGF7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDAzMjQ1O1xyXG59XHJcbi5tYXQtZWxldmF0aW9uLXo4e1xyXG5wYWRkaW5nOiAyJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/layout/screen1/screen1.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/screen1/screen1.component.ts ***!
  \*****************************************************/
/*! exports provided: Screen1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1Component", function() { return Screen1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ELEMENT_DATA = [
    { fechaIni: '01-01-2019 20:0:0', idalarma: 1, slot: 4, codivr: 100, summary: 'prueba', totalclientes: 1 },
    { fechaIni: '01-01-2019 20:0:0', idalarma: 2, slot: 4, codivr: 100, summary: 'prueba', totalclientes: 2 },
    { fechaIni: '01-01-2019 20:0:0', idalarma: 3, slot: 4, codivr: 100, summary: 'prueba', totalclientes: 3 },
    { fechaIni: '01-01-2019 20:0:0', idalarma: 4, slot: 4, codivr: 100, summary: 'prueba', totalclientes: 1 },
    { fechaIni: '01-01-2019 20:0:0', idalarma: 5, slot: 4, codivr: 100, summary: 'prueba', totalclientes: 2 }
];
var Screen1Component = /** @class */ (function () {
    function Screen1Component() {
        this.displayedColumns = ['fechainicio', 'id_alarma', 'slot', 'cod_ivr', 'summary', 'totalclientes'];
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatTableDataSource"](ELEMENT_DATA);
        this.places = [];
        this.places = [
            {
                imgSrc: 'assets/images/card-1.jpg',
                place: 'Cozy 5 Stars Apartment',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
                charge: '$899/night',
                location: 'Barcelona, Spain'
            },
            {
                imgSrc: 'assets/images/card-2.jpg',
                place: 'Office Studio',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in London, UK.',
                charge: '$1,119/night',
                location: 'London, UK'
            },
            {
                imgSrc: 'assets/images/card-3.jpg',
                place: 'Beautiful Castle',
                description: 
                // tslint:disable-next-line:max-line-length
                'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Milan.',
                charge: '$459/night',
                location: 'Milan, Italy'
            }
        ];
    }
    Screen1Component.prototype.applyFilter = function (filterValue) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.dataSource.filter = filterValue;
    };
    Screen1Component.prototype.ngOnInit = function () { };
    Screen1Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-screen1',
            template: __webpack_require__(/*! ./screen1.component.html */ "./src/app/layout/screen1/screen1.component.html"),
            styles: [__webpack_require__(/*! ./screen1.component.scss */ "./src/app/layout/screen1/screen1.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], Screen1Component);
    return Screen1Component;
}());



/***/ }),

/***/ "./src/app/layout/screen1/screen1.module.ts":
/*!**************************************************!*\
  !*** ./src/app/layout/screen1/screen1.module.ts ***!
  \**************************************************/
/*! exports provided: Screen1Module */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Screen1Module", function() { return Screen1Module; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/esm5/grid-list.es5.js");
/* harmony import */ var _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/modules/stat/stat.module */ "./src/app/shared/modules/stat/stat.module.ts");
/* harmony import */ var _screen1_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./screen1-routing.module */ "./src/app/layout/screen1/screen1-routing.module.ts");
/* harmony import */ var _screen1_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./screen1.component */ "./src/app/layout/screen1/screen1.component.ts");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/esm5/paginator.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _components_consultar2_sidebar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../components/consultar2/sidebar.component */ "./src/app/layout/components/consultar2/sidebar.component.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var Screen1Module = /** @class */ (function () {
    function Screen1Module() {
    }
    Screen1Module = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _screen1_routing_module__WEBPACK_IMPORTED_MODULE_6__["Screen1RoutingModule"],
                _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _shared_modules_stat_stat_module__WEBPACK_IMPORTED_MODULE_5__["StatModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _screen1_routing_module__WEBPACK_IMPORTED_MODULE_6__["Screen1RoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatListModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_12__["TranslateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSelectModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["FlexLayoutModule"].withConfig({ addFlexToParent: false })
            ],
            declarations: [_screen1_component__WEBPACK_IMPORTED_MODULE_7__["Screen1Component"], _components_consultar2_sidebar_component__WEBPACK_IMPORTED_MODULE_11__["Consultar2"]]
        })
    ], Screen1Module);
    return Screen1Module;
}());



/***/ })

}]);
//# sourceMappingURL=screen1-screen1-module.js.map