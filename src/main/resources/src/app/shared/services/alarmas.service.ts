
import { Injectable, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import {environment} from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { PeriodicElement } from 'src/app/layout/screen1/screen1.component';

var dat;
const APIAlarmsEndpoint = environment.APIGetAlarmsEndpoint;
@Injectable({
  providedIn: 'root'
})
export class AlarmasService {
  err;
  dataTable;
  url = APIAlarmsEndpoint;    
  constructor(private http:HttpClient) { }
  paginator: MatPaginator;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataTable.filter = filterValue;

}

  consultar(form){
    //TODO
    //consulta http y mapeo de la data

 this.http.post(this.url,form)
      .subscribe(
        data => {
          console.log('data', data);
          dat=data['listDetalleAlarma'];
          this.dataTable = new MatTableDataSource<PeriodicElement>(dat);
          this.dataTable.paginator = this.paginator;

        },
        (error:Response) => {
          console.log('status',error);
            this.err=error.status;
        }
      );
  }
}

