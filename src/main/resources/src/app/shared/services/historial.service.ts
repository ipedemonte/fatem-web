import { Injectable } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import {environment} from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { PeriodicElement } from 'src/app/layout/dashboard/dashboard.component';

var dat;
const APIHistoryEndpoint = environment.APIHistoryEndpoint;
@Injectable({
  providedIn: 'root'
})
export class HistorialService {
  err;
  dataTable;
  url = APIHistoryEndpoint;    
  constructor(private http:HttpClient) { }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataTable.filter = filterValue;

}

  consultar(form){
    //TODO
    //consulta http y mapeo de la data

 this.http.post(this.url,form)
      .subscribe(
        data => {
          console.log('data', data);
          dat=data['listDetalleAlarma'];
          this.dataTable = new MatTableDataSource<PeriodicElement>(dat);
        },
        (error:Response) => {
          console.log('status',error);
            this.err=error.status;
        }
      );
  }
}

