import { Injectable } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
//import { HttpClient } from '@angular/common/http';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

var dat;
const APILoginEndpoint = environment.APILoginEndpoint;
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  err;
  usuario;
  url = APILoginEndpoint; 

  constructor(private http:HttpClient,private router:Router) { }
  login(form){
    //TODO
    //consulta http y mapeo de la data
    
    this.http.post(this.url,form)
    .subscribe(
      data => {
        console.log('data', JSON.stringify(data));
        dat=data['user'];
        this.usuario = dat;
        console.log(this.usuario);
        localStorage.setItem('isLoggedin', 'true');
        this.router.navigate(["/dashboard"]);
      },
      (error:Response) => {
        console.log('status',error);
          this.err=error.status;

      }

    );

    

  }


}

