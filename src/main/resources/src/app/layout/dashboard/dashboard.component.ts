import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { HistorialService } from 'src/app/shared/services/historial.service';

export interface PeriodicElement {
    elemento: string;
    planta: string;
    hostname: string;
    zona: string;
    cantidad:number;
  }
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    providers:[HistorialService]
})
export class DashboardComponent implements OnInit {

    displayedColumns : string[] = ['elemento', 'planta', 'hostname', 'zona', 'cantidad'];

    constructor(private historial: HistorialService) {
        
    }

    ngOnInit() {}
}