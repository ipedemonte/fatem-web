import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { AlarmasService } from '../../shared/services/alarmas.service';
/**
 * @title Table with pagination
 */
@Component({
    selector: 'app-screen1',
  templateUrl: './screen1.component.html',
  styleUrls: ['./screen1.component.scss']
})
export class Screen1Component implements OnInit {

  displayedColumns:string[] = ['id', 'fechaevento', 'gravedad', 'zona', 'elemento','diagnostico','planta','nombre', 'totalclientes'];


  ngOnInit() {
    
  }
  constructor(private alarmas: AlarmasService) {
}
  
    //TODO
    //consulta http y mapeo de la data
    
 


}

export interface PeriodicElement {
    id: number;
    fechaevento: string;
    gravedad: string;
    zona: string;
    elemento:string;
    diagnostico:string;
    planta:string;
    nombre:string;
    totalclientes:number;
}


