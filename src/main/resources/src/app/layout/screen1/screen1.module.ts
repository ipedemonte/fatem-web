import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';

import { StatModule } from '../../shared/modules/stat/stat.module';
import { Screen1RoutingModule } from './screen1-routing.module';
import { Screen1Component } from './screen1.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import { Form2 } from '../components/form2/form2.component';
import {
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
    
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { MatSelectModule} from '@angular/material';







@NgModule({
    imports: [
        CommonModule,
        Screen1RoutingModule,
        MatGridListModule,
        StatModule,
        MatCardModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatPaginatorModule,
        FormsModule,
        MatFormFieldModule,
        CommonModule,
        Screen1RoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        FormsModule    ,
        MatFormFieldModule,
        MatSelectModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [Screen1Component,Form2]
})
export class Screen1Module {}
