import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Form1 } from './form1.component';

describe('Form1', () => {
    let component: Form1;
    let fixture: ComponentFixture<Form1>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Form1]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Form1);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
