import { Component, OnInit } from '@angular/core';
import {HistorialService} from '../../../shared/services/historial.service';


export interface Food {

  }
@Component({
    selector: 'app-form1',
    templateUrl: './form1.component.html',
    styleUrls: ['./form1.component.scss']
})

export class Form1 {
         
  form={
    fecha_inicio: "",
    fecha_fin: ""
  }

    consultar(){ 
      this.historial.consultar(this.form);
     
  }
  constructor(
    private historial:HistorialService,
    ) { }
}
