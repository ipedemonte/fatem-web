import { Component, OnInit } from '@angular/core';

export interface Food {
    value: string;
    viewValue: string;
  }
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent {
    foods: Food[] = [
        {value: 'FO', viewValue: 'FO'},
        {value: 'CU', viewValue: 'CU'}

      ];
     
}
