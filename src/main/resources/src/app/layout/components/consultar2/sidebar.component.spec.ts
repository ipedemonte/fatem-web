import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Consultar2 } from './sidebar.component';

describe('Consultar2', () => {
    let component: Consultar2;
    let fixture: ComponentFixture<Consultar2>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Consultar2]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Consultar2);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
