import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Form2 } from './form2.component';

describe('Form2', () => {
    let component: Form2;
    let fixture: ComponentFixture<Form2>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [Form2]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(Form2);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
