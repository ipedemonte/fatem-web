import { Component, OnInit, InjectionToken } from '@angular/core';
import {AlarmasService} from '../../../shared/services/alarmas.service';

@Component({
    selector: 'app-form2',
    templateUrl: './form2.component.html',
    styleUrls: ['./form2.component.scss']
})

export class Form2 {

  form={
    zona: "",
    planta: "",
    elemento:""
    }

    consultar(){ 
      this.alarmas.consultar(this.form);
     
  }
  constructor(
    private alarmas:AlarmasService,
    ) { }




}

