import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule
    
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { Form1 } from './components/form1/form1.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
import { Screen2Component } from './screen2/screen2.component';
import { FormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import { setBindingRoot } from '@angular/core/src/render3/state';
import { MatSelectModule} from '@angular/material';
import {CarouselComponent} from '../layout/carousel/carousel.component';




@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule,
        FormsModule    ,
        MatFormFieldModule,
        MatSelectModule
    ],
    declarations: [LayoutComponent, NavComponent, NavbarComponent]

})
export class LayoutModule { }
