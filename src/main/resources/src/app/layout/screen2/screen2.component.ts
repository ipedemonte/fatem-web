import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { HistorialService } from 'src/app/shared/services/historial.service';

export interface PeriodicElement {
  elemento: string;
  planta: string;
  hostname: string;
  zona: string;
  cantidad:number;
}


@Component({
  selector: 'app-screen2',
  templateUrl: './screen2.component.html',
  styleUrls: ['./screen2.component.scss']
})
export class Screen2Component implements OnInit {

    displayedColumns = ['elemento', 'planta', 'hostname', 'zona', 'cantidad'];

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        //this.dataSource.filter = filterValue;
    }

    constructor(private historial: HistorialService) {
        
    }

    ngOnInit() {}
}